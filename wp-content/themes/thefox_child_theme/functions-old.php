<?php
function theme_enqueue_styles() {

    global $rd_data;

    wp_enqueue_style( 'style', get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'style_end', get_template_directory_uri() . '/style_end.css' );
    //wp_enqueue_style( 'placeholder', get_stylesheet_directory_uri() . '/css/placeholder-loading.min.css' );
    if( rd_check_woo_status() == true) {
		define( 'WOOCOMMERCE_USE_CSS', false );

		wp_dequeue_style( 'rd_woocommerce');
		if ($rd_data['rd_shop_design']== 'classic'){
		wp_enqueue_style( 'rd_woocommerce', get_template_directory_uri(). '/css/woocommerce.css');
		}
		else{
		wp_enqueue_style( 'rd_woocommerce', get_template_directory_uri(). '/css/woocommerce_trending.css');
		}
    }
	if($rd_data['rd_responsive']== true){
	if ($rd_data['rd_nav_type'] !== 'nav_type_19' && $rd_data['rd_nav_type'] !== 'nav_type_19_f' ){
	    wp_enqueue_style('media-queries',  get_template_directory_uri() . '/media-queries_wide.css');
	}
	else {
	    wp_enqueue_style('media-queries',  get_template_directory_uri() . '/media-queries_edge_nav.css');
	}
	}
	
	wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/style.css', array( 'style' ));
		
    wp_enqueue_style( 'slick-style', get_stylesheet_directory_uri() . '/css/slick.css', array( 'style' ));
	
	wp_enqueue_style( 'slick-theme', get_stylesheet_directory_uri() . '/css/slick-theme.css', array( 'style' ));
	
	wp_enqueue_script( 'child-custom-js', get_stylesheet_directory_uri() . '/js/customjs.js', array( 'jquery' ));
	
	wp_enqueue_script( 'boxslider-js', get_stylesheet_directory_uri() . '/js/jquery.bxslider.js', array( 'jquery' ), '1.0.1', true);
	
	wp_enqueue_script( 'slick-js', get_stylesheet_directory_uri() . '/js/slick.js', array( 'jquery' ), '', true);
	
	if(is_page('about')){
		 wp_enqueue_style('timeline',  get_stylesheet_directory_uri() . '/css/timeline-css/style.css');
		wp_enqueue_script( 'timeline-main-js', get_stylesheet_directory_uri() . '/js/timeline.js', array( 'jquery' ));


		wp_enqueue_script( 'waypoint', get_stylesheet_directory_uri() . '/js/jquery.waypoints.min.js', array( 'jquery' ), '', true);
		wp_enqueue_script( 'timeline2', get_stylesheet_directory_uri() . '/js/timeline2.js', array( 'jquery','waypoint' ), '', true);
	}

}
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );


///////////////////////////////////////////////////////////////////////////////////////
//////                                                                           /////
/////                   ADD PRICING TABLE 2   BY JBM                            /////
////                                                                           /////
///////////////////////////////////////////////////////////////////////////////////
add_action( 'vc_before_init', 'rd_pricing_table2' );
function rd_pricing_table2() {
		

   vc_map( array(
      "name" => __("Pricing Table 2",'thefoxwp'),
      "base" => "pricetable_sc2",
	"icon"		=> get_template_directory_uri() . "/images/vc_icons/vc_pricing_table.png",
      "class" => "",
	  "weight" => "80",
      "category" => __('Content','thefoxwp'),
      "params" => array(
         array(
            "type" => "dropdown",
			"admin_label" => true,
            "class" => "",
            "heading" => __("Pricing Table Style",'thefoxwp'),
            "param_name" => "pt_style",
            "value" => array("New Style 1" => "jm_pt_1",),
            "description" => __("Choose the Pricing Table Design",'thefoxwp')
         ),  
  		 	
         array(
            "type" => "dropdown",
			"admin_label" => true,
            "class" => "",
            "heading" => __("Pricing Table",'thefoxwp'),
            "param_name" => "id",
            "value" => PTid(), 
            "description" => __("Choose the Pricing Table to use",'thefoxwp')
         ),
        
      )
   ) );
}


if (!function_exists('pricetable_sc2')) {
function pricetable_sc2($atts, $content = null) {  
    extract(shortcode_atts(array(  
		'margin_top'   => '0',

		'margin_bottom' => '0',
		
		'pt_style' => 'jm_pt_1',
	
		'id' => '',
		'mt' => '0',
		'mb' => '0',
		
		'color_1' => '',
		'color_2' => '',
		'color_3' => '',
		'color_4' => '',
		'color_5' => '',
		'color_rec' => '',
		'animation' => '',

   ), $atts));

global $rd_data;

$pt_id = RandomString(20);

$output = '<style type="text/css" >';

	$output .= '#pt_'.$pt_id.'.rd_pt_1 .pt_col_nb_0 .pricetable-button-container a,#pt_'.$pt_id.'.rd_pt_1 .pt_col_nb_0 h3.pricetable-name{background:'.$color_1.'; }#pt_'.$pt_id.'.rd_pt_1 .pt_col_nb_0.pricetable-featured .pricetable-column-inner{border-color:'.$color_1.';}';	
	$output .= '#pt_'.$pt_id.'.rd_pt_1 .pt_col_nb_1 .pricetable-button-container a,#pt_'.$pt_id.'.rd_pt_1 .pt_col_nb_1 h3.pricetable-name{background:'.$color_2.'; }#pt_'.$pt_id.'.rd_pt_1 .pt_col_nb_1.pricetable-featured .pricetable-column-inner{border-color:'.$color_2.';}';		
	$output .= '#pt_'.$pt_id.'.rd_pt_1 .pt_col_nb_2 .pricetable-button-container a,#pt_'.$pt_id.'.rd_pt_1 .pt_col_nb_2 h3.pricetable-name{background:'.$color_3.'; }#pt_'.$pt_id.'.rd_pt_1 .pt_col_nb_2.pricetable-featured .pricetable-column-inner{border-color:'.$color_3.';}';		
	$output .= '#pt_'.$pt_id.'.rd_pt_1 .pt_col_nb_3 .pricetable-button-container a,#pt_'.$pt_id.'.rd_pt_1 .pt_col_nb_3 h3.pricetable-name{background:'.$color_4.'; }#pt_'.$pt_id.'.rd_pt_1 .pt_col_nb_3.pricetable-featured .pricetable-column-inner{border-color:'.$color_4.';}';	
	$output .= '#pt_'.$pt_id.'.rd_pt_1 .pt_col_nb_4 .pricetable-button-container a,#pt_'.$pt_id.'.rd_pt_1 .pt_col_nb_4 h3.pricetable-name{background:'.$color_5.'; }#pt_'.$pt_id.'.rd_pt_1 .pt_col_nb_4.pricetable-featured .pricetable-column-inner{border-color:'.$color_5.';}';		
	$output .= '#pt_'.$pt_id.'.rd_pt_1 .pricetable-featured h4.pricetable-price{background:'.$color_rec.'; color:#fff; }#pt_'.$pt_id.'.rd_pt_1 .pricetable-featured  .pricetable-button-container a:hover{background:'.$color_rec.' !important;}';

$output .= '</style>';
$output .= '<div class="'.$pt_style.' '.$animation.'" id="pt_'.$pt_id.'">' . do_shortcode('[price_table id='.$id.']') . '</div>';

return $output;
}

add_shortcode("pricetable_sc2", "pricetable_sc2");
}



///////////////////////////////////////////////////////////////////////////////////////
//////                                                                           /////
/////                          ADD SVG ICON BOX	BY JBM 		                    /////
////                                                                           /////
///////////////////////////////////////////////////////////////////////////////////
add_action( 'vc_before_init', 'rd_svg_icon_para_sc' );
function rd_svg_icon_para_sc() {
		

   vc_map( array(
      "name" => __("Paragraph with SVG icon",'thefoxwp'),
      "base" => "iconbox_svg",
	"icon"		=> get_template_directory_uri() . "/images/vc_icons/vc_paragraph_icon.png",
      "class" => "",
	  "weight" => "71",
      "category" => __('Content','thefoxwp'),
      "params" => array( 
         array(
            "type" => "dropdown",
            "class" => "",
            "heading" => __("CSS Animation",'thefoxwp'),
            "param_name" => "animation",
			'value' => array(__("No",'thefoxwp') => "",__("Fade In",'thefoxwp') => "rda_fadeIn",__("Fade Top to Bottom",'thefoxwp') => "rda_fadeInDown", __("Fade Bottom to Top",'thefoxwp') => "rda_fadeInUp", __("Fade Left to Right",'thefoxwp') => "rda_fadeInLeft", __("Fade Right to Left",'thefoxwp') => "rda_fadeInRight", __("Bounce In",'thefoxwp') => "rda_bounceIn",__("Bounce Top to Bottom",'thefoxwp') => "rda_bounceInDown",__("Bounce Bottom to Top",'thefoxwp') => "rda_bounceInUp", __("Bounce Left to Right",'thefoxwp') => "rda_bounceInLeft", __("Bounce Right to Left",'thefoxwp') => "rda_bounceInRight", __("Zoom In",'thefoxwp') => "rda_zoomIn", __("Flip Vertical",'thefoxwp') => "rda_flipInX",__("Flip Horizontal",'thefoxwp') => "rda_flipInY", __("Bounce",'thefoxwp') => "rda_bounce", __("Flash",'thefoxwp') => "rda_flash",__("Shake",'thefoxwp') => "rda_shake",__( "Pulse",'thefoxwp') => "rda_pulse",__( "Swing",'thefoxwp') => "rda_swing", __("Rubber band",'thefoxwp') => "rda_rubberBand", __("Wobble",'thefoxwp') => "rda_wobble", __("Tada!",'thefoxwp') => 'rda_tada'),
			"description" => __("Select type of animation if you want this element to be animated when it enters into the browsers viewport. Note: Works only in modern browsers.",'thefoxwp')
			),  		 
		 array(
            "type" => "textfield",
            "class" => "",
            "heading" => __("Top Margin",'thefoxwp'),
            "param_name" => "mt",
            "value" => "0",
            "description" => __("Top margin for the Box (e.g 20 )",'thefoxwp')
         ), 
		 array(
            "type" => "textfield",
            "class" => "",
            "heading" => __("Bottom Margin",'thefoxwp'),
            "param_name" => "mb",
            "value" => "0",
            "description" => __("Bottom margin for the Box (e.g 20 )",'thefoxwp')
         ), 
		 array(
            'type' => 'attach_image',
			"class" => "",
			"heading" => __("Upload SVG Icon:", 'thefoxwp'),
			"param_name" => "icon_svg",
			"admin_label" => true,
			"value" => "fa-cog",
			//"description" => __("Upload svg icon", 'thefoxwp'),		
    	     ),
		   array(
            'type' => 'textfield',
			"class" => "",
			"heading" => __("Set svg icon width", 'thefoxwp'),
			"param_name" => "icon_svg_width",
			"admin_label" => true,
			"description" => __("Set svg icon width in px ex. 35px", 'thefoxwp'),		
    	     ),
		  array(
            'type' => 'textfield',
			"class" => "",
			"heading" => __("Custom class", 'thefoxwp'),
			"param_name" => "icon_svg_custom_class",
			"admin_label" => true,
			"description" => __("Add svg icon custom class", 'thefoxwp'),		
    	     ),
		 array(
            "type" => "textfield",
			"admin_label" => true,
            "class" => "",
            "heading" => __("Heading",'thefoxwp'),
            "param_name" => "title",
            "value" => __("Awesome heading!",'thefoxwp'),
            "description" => __("Enter the heading",'thefoxwp')
         ),
		  array(
            "type" => "colorpicker",
            "class" => "",
            "heading" => __("Heading color",'thefoxwp'),
            "param_name" => "t_color",
            "value" => '', //Default Red color
            "description" => __("Optional",'thefoxwp'),	
         ), 
         array(
            "type" => "textarea_html",
            "class" => "",
            "heading" => __("Main text",'thefoxwp'),
            "param_name" => "content",
            "value" => __('Awesome main text goes here','thefoxwp'), 
            "description" => __("Enter the main text",'thefoxwp')
         ),		 
		  array(
            "type" => "colorpicker",
            "class" => "",
            "heading" => __("Main text color",'thefoxwp'),
            "param_name" => "content_color",
            "value" => '', //Default Red color
            "description" => __("Optional",'thefoxwp'),	
         ),  
		 array(
            "type" => "textfield",
            "class" => "",
            "heading" => __("Button text",'thefoxwp'),
            "param_name" => "button_text",
            "value" => "",
            "description" => __("Enter if you want to use a button",'thefoxwp')
         ), 
		  array(
            "type" => "colorpicker",
            "class" => "",
            "heading" => __("Button text color",'thefoxwp'),
            "param_name" => "button_color",
            "value" => '', //Default Red color
            "description" => __("Optional",'thefoxwp'),	
		 	'dependency' => array( 'element' => 'button_text', 'not_empty' => true)	
         ),	
		 array(
            "type" => "textfield",
            "class" => "",
            "heading" => __("Link",'thefoxwp'),
            "param_name" => "link",
            "value" => "",
            "description" => __("Enter if you want to put a link for the box",'thefoxwp')
         ),		 
         array(
			'type' => 'dropdown',
            "class" => "",
            "heading" => __("Open link in new tab?",'thefoxwp'),
            "param_name" => "target",			
		 	'dependency' => array( 'element' => 'link', 'not_empty' => true),	
            'value' => array(  'Yes'  => '_blank', 'No'  => '_self' ),
	        "description" => __("Select if you want to open the link in a new tab",'thefoxwp')
         ),	 
		 array(
  		 	'type' => 'checkbox',
			'heading' => __("Change color on hover?",'thefoxwp'	),
			'param_name' => 'change_hover',
			'value' => array(  'Yes'  => 'yes' ),
			'description' => __("Select if you want the box to change color on hover",'thefoxwp'	),
		),
			
		  array(
            "type" => "colorpicker",
            "class" => "",
            "heading" => __("Hover Icon color",'thefoxwp'),
            "param_name" => "hover_i_color",
            "value" => '', //Default Red color
            "description" => __("Optional",'thefoxwp'),	
		 	'dependency' => array( 'element' => 'change_hover', 'not_empty' => true)		
         ),		  
			 
		  array(
            "type" => "colorpicker",
            "class" => "",
            "heading" => __("Hover Heading color",'thefoxwp'),
            "param_name" => "hover_t_color",
            "value" => '', //Default Red color
            "description" => __("Optional",'thefoxwp'),	
		 	'dependency' => array( 'element' => 'change_hover', 'not_empty' => true)	
         ), 
		  array(
            "type" => "colorpicker",
            "class" => "",
            "heading" => __("Hover Text color",'thefoxwp'),
            "param_name" => "hover_text_color",
            "value" => '', //Default Red color
            "description" => __("Optional",'thefoxwp'),	
		 	'dependency' => array( 'element' => 'change_hover', 'not_empty' => true)	
         ), 
		  array(
            "type" => "colorpicker",
            "class" => "",
            "heading" => __("Hover Button text color",'thefoxwp'),
            "param_name" => "hover_button_color",
            "value" => '', //Default Red color
            "description" => __("Optional",'thefoxwp'),	
		 	'dependency' => array( 'element' => 'change_hover', 'not_empty' => true)	
         ),		  

      )
   ) );
}




if (!function_exists('iconbox_svg')) {
function iconbox_svg( $atts, $content = null ) {
	
	global $rd_data;
	$box_id = RandomString(20);
	
	extract(shortcode_atts(array(

		'title'   => 'Awesome heading!',

		't_color' => '',

		'target' => '_blank',

		'i_color' => '',
		
		'i_bg_color' => '',
		
		'i_b_color' => '',

		'type' => '10',

		'link' => '',
		
		'content_color' => '',	

		'icon_svg'	=> 'fa-cog',
						
		'button_text' => '',
		
		'button_color' => '',
		
		'change_hover' => '',
		
		'hover_i_color' => '',
		
		'hover_i_bg_color' => '',
		
		'hover_i_b_color' => '',
		
		'hover_t_color' => '',
		
		'hover_text_color' => '',
		
		'hover_button_color' => '',
		'mt' => '0',
		
		'mb' => '0',
		'animation' => '',
		
		'icon_svg_width'	=> '',
		
		'icon_svg_custom_class' => '',

    ), $atts));


if($t_color == '' ){ $t_color = $rd_data['rd_content_heading_color'];}
if($i_color == '' ){ $i_color = $rd_data['rd_content_hl_color'];}
if($content_color == '' ){ $content_color = $rd_data['rd_content_text_color'];}


$output ='<style type="text/css" >#rand_'.$box_id.' {margin-top:'.$mt.'px; margin-bottom:'.$mb.'px; }</style>';

	if ( ! empty( $atts['icon'] ) ) {
            // Don't load the CSS files to trim loading time, include the specific styles via PHP
            // wp_enqueue_style( '4k-icon-' . $cssFile, plugins_url( 'icons/css/' . $cssFile . '.css', __FILE__ ) );
			$cssFile = substr( $atts['icon'], 0, stripos( $atts['icon'], '-' ) );
			wp_enqueue_style( '4k-icons',  RD_DIRECTORY . '/includes/4k-icons/css/icon-styles.css' , null, VERSION_GAMBIT_VC_4K_ICONS );
			wp_enqueue_script( '4k-icons',  RD_DIRECTORY . '/includes/4k-icons/js/script-ck.js', array( 'jquery' ), VERSION_GAMBIT_VC_4K_ICONS, true );
		}
  global $iconContents;

        include('icon-contents.php' );

		// Normal styles used for everything
        $cssFile = substr( $atts['icon'], 0, stripos( $atts['icon'], '-' ) );

        $iconFile =  RD_DIRECTORY . '/includes/4k-icons/icons/fonts/' . $cssFile;
		$iconFile = apply_filters( '4k_icon_font_pack_path', $iconFile, $cssFile );

		// Fix ligature icons (these are icons that use more than 1 symbol e.g. mono social icons)
		$ligatureStyle = '';
        if ( $cssFile == 'mn' ) {
            $ligatureStyle = '-webkit-font-feature-settings:"liga","dlig";-moz-font-feature-settings:"liga=1, dlig=1";-moz-font-feature-settings:"liga","dlig";-ms-font-feature-settings:"liga","dlig";-o-font-feature-settings:"liga","dlig";
                         	 font-feature-settings:"liga","dlig";
                        	 text-rendering:optimizeLegibility;';
        }

		$iconCode = '';
		if ( ! empty( $atts['icon'] ) ) {
			if( ! empty ($iconContents[ $atts['icon'] ])){
			$iconCode = $iconContents[ $atts['icon'] ];
			}
		}

		$ret = "<style type='text/css'>
            @font-face {
            	font-family: '" . $cssFile . "';
            	src:url('" . $iconFile . ".eot');
            	src:url('" . $iconFile . ".eot?#iefix') format('embedded-opentype'),
            		url('" . $iconFile . ".woff') format('woff'),
            		url('" . $iconFile . ".ttf') format('truetype'),
            		url('" . $iconFile . ".svg#oi') format('svg');
            	font-weight: normal;
            	font-style: normal;
            }
            #rand_".$box_id." ." . $atts['icon'] . ":before { font-family: '" . $cssFile . "'; font-weight: normal; font-style: normal;}";
		if( !empty ($iconCode) ){
		$ret .= "#rand_".$box_id." .". $atts['icon'] . ":before { content: \"" . $iconCode . "\"; $ligatureStyle }";
		}

		$ret .= "</style>";

		// Compress styles a bit for readability
		$ret = preg_replace( "/\s?(\{|\})\s?/", "$1",
			preg_replace( "/\s+/", " ",
			str_replace( "\n", "", $ret ) ) )
			. "\n";



// if($type == '10'){
	
	$output .= $ret;
	
if($change_hover !== ''){
	
		if($hover_t_color == '' ){ $hover_t_color = $rd_data['rd_content_hl_color'];}
		
		if($hover_i_color == '' ){ $hover_i_color = $rd_data['rd_content_hl_color'];}
				
		if($hover_text_color == '' ){ $hover_boxbg_color = '';}

		if($hover_button_color == '' ){ $hover_button_color = '#2d3e50';}

	$output .= '<style type="text/css" >#rand_'.$box_id.':hover i{color:'.$hover_i_color.'!important;}#rand_'.$box_id.':hover h3{color:'.$hover_t_color.'!important;}#rand_'.$box_id.':hover p{color:'.$hover_text_color.'!important;}#rand_'.$box_id.':hover .icon_box_button{color:'.$hover_button_color.'!important;}</style>';

	
}
	
	$img_id = preg_replace( '/[^\d]/', '', $icon_svg );
	// $img = wpb_getImageBySize( array( 'attach_id' => $img_id, 'thumb_size' => '250'  ) );
	
	// var_dump($img);
	$img = wp_get_attachment_image_src($img_id, 'full');
	$svgWidth = ($icon_svg_width) ? $icon_svg_width : '60px';

	if($link !== ''){$output .= '<a href="'.$link.'" target="'.$target.'" >';}
	
	$output .= '<div id="rand_'.$box_id.'" class="icon_box_si '.$animation.'">';
	$output .= '<span class="svg_icon"><img class="'.$icon_svg_custom_class.'" style="width:'.$svgWidth.'" src='.$img[0].'></span><h3 style="color:'.$t_color.';margin-left:8px;padding-left:'.$svgWidth.'">'. do_shortcode($title) .'</h3><p style="color:'.$content_color.';margin-left:8px;padding-left:'.$svgWidth.'">'. do_shortcode($content) .'</p>';


if($button_text !== ''){
	$output .= '<a class="icon_box_button" style="color:'.$button_color.'"; href="'.$link.'" target="'.$target.'" >'.$button_text.'</a>';
}
	$output .= '</div>';
	if($link !== ''){$output .= '</a>'; }

	return $output;


//}


}


add_shortcode('iconbox_svg', 'iconbox_svg');
}

///////////////////////////////////////////////////////////////////////////////////////
//////                                                                           /////
/////                ALLOWS SVG UPLOAD IN WP MEDIA LIB	BY JBM 		            /////
////                                                                           /////
///////////////////////////////////////////////////////////////////////////////////
function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');


///////////////////////////////////////////////////////////////////////////////////////
//////                                                                           /////
/////                 OVERRIDE CALL TO ACTION BOX   BY JBM                      /////
////                                                                           /////
///////////////////////////////////////////////////////////////////////////////////
add_action( 'vc_after_init', 'vc_after_init_rd_cta_sc' );
 
function vc_after_init_rd_cta_sc() {

   vc_map( array(
      "name" => __("Promo box",'thefoxwp'),
      "base" => "rd_cta",
	"icon"		=> get_template_directory_uri() . "/images/vc_icons/vc_promo_box.png",
      "class" => "",
	  "weight" => "64",
      "category" => __('Content','thefoxwp'),
      "params" => array(
	       array(
            "type" => "dropdown",
            "class" => "",	
            "heading" => __("Promo box style",'thefoxwp'),
            "param_name" => "style",
            "value" => array ("Style 1" => "rd_cta_1", "Style 2" => "rd_cta_2", "Style 3" => "rd_cta_3", "Custom style(w/ svg icon)" => "jm_custom_style" ), 
            "description" => __("Choose Box Design",'thefoxwp')
         ),  
         array(
            "type" => "dropdown",
            "class" => "",
            "heading" => __("CSS Animation",'thefoxwp'),
            "param_name" => "animation",
			'value' => array(__("No",'thefoxwp') => "",__("Fade In",'thefoxwp') => "rda_fadeIn",__("Fade Top to Bottom",'thefoxwp') => "rda_fadeInDown", __("Fade Bottom to Top",'thefoxwp') => "rda_fadeInUp", __("Fade Left to Right",'thefoxwp') => "rda_fadeInLeft", __("Fade Right to Left",'thefoxwp') => "rda_fadeInRight", __("Bounce In",'thefoxwp') => "rda_bounceIn",__("Bounce Top to Bottom",'thefoxwp') => "rda_bounceInDown",__("Bounce Bottom to Top",'thefoxwp') => "rda_bounceInUp", __("Bounce Left to Right",'thefoxwp') => "rda_bounceInLeft", __("Bounce Right to Left",'thefoxwp') => "rda_bounceInRight", __("Zoom In",'thefoxwp') => "rda_zoomIn", __("Flip Vertical",'thefoxwp') => "rda_flipInX",__("Flip Horizontal",'thefoxwp') => "rda_flipInY", __("Bounce",'thefoxwp') => "rda_bounce", __("Flash",'thefoxwp') => "rda_flash",__("Shake",'thefoxwp') => "rda_shake",__( "Pulse",'thefoxwp') => "rda_pulse",__( "Swing",'thefoxwp') => "rda_swing", __("Rubber band",'thefoxwp') => "rda_rubberBand", __("Wobble",'thefoxwp') => "rda_wobble", __("Tada!",'thefoxwp') => 'rda_tada'),
            "description" => __("Select type of animation if you want this element to be animated when it enters into the browsers viewport. Note: Works only in modern browsers.",'thefoxwp')
         ),	 
		 array(
            "type" => "textfield",
            "class" => "",
            "heading" => __("Box Title",'thefoxwp'),
            "param_name" => "title",
            "value" => __("Box Title",'thefoxwp'),
            "description" => __("Enter the Title for the Box",'thefoxwp')
         ),  
		  array(
            "type" => "colorpicker",
            "class" => "",
            "heading" => __("Title color",'thefoxwp'),
            "param_name" => "title_color",
            "value" => '', //Default Red color
            "description" => __("Choose the Title color",'thefoxwp'),	
         ), 
         array(
            "type" => "textarea",
			"admin_label" => true,
            "class" => "",
            "heading" => __("Box Text",'thefoxwp'),
            "param_name" => "content",
            "value" => __('This is the main text','thefoxwp'), 
            "description" => __("Heading for the module",'thefoxwp')
         ),				 
		  array(
            "type" => "colorpicker",
            "class" => "",
            "heading" => __("Text color",'thefoxwp'),
            "param_name" => "text_color",
            "value" => '', //Default Red color
            "description" => __("Choose the Text color",'thefoxwp'),	
         ),		 
		  array(
            "type" => "colorpicker",
            "class" => "",
            "heading" => __("Box Background color",'thefoxwp'),
            "param_name" => "bg_color",
            "value" => '', //Default Red color
            "description" => __("Choose the box background color",'thefoxwp'),
			'dependency' =>  array( 'element' => 'style', 'value' => array('rd_cta_1'))
         ),		 
		  array(
            "type" => "colorpicker",
            "class" => "",
            "heading" => __("Box Border color",'thefoxwp'),
            "param_name" => "border_color",
            "value" => '', //Default Red color
            "description" => __("Choose the box border color",'thefoxwp'),
			'dependency' =>  array( 'element' => 'style', 'value' => array('rd_cta_1'))	  
         ),		 		 
		  array(
            "type" => "colorpicker",
            "class" => "",
            "heading" => __("Box Left Border color",'thefoxwp'),
            "param_name" => "left_border_color",
            "value" => '', //Default Red color
            "description" => __("Choose the box left border color",'thefoxwp'),
		 	'dependency' =>  array( 'element' => 'style', 'value' => array('rd_cta_2'))
         ),
		 array(
            "type" => "textfield",
            "class" => "",
            "heading" => __("Button text",'thefoxwp'),
            "param_name" => "button_text",
            "value" => "",
            "description" => __("Enter the button text",'thefoxwp'),
		 	'dependency' =>  array( 'element' => 'style', 'value' => array('rd_cta_1','rd_cta_2'))
         ),
		 array(
            "type" => "textfield",
            "class" => "",
            "heading" => __("Button link",'thefoxwp'),
            "param_name" => "button_link",
            "value" => "",
            "description" => __("Enter the link for the button",'thefoxwp'),
		 	'dependency' =>  array( 'element' => 'style', 'value' => array('rd_cta_1','rd_cta_2'))
         ),
		          array(
			'type' => 'dropdown',
            "class" => "",
            "heading" => __("Open link in new tab?",'thefoxwp'),
            "param_name" => "target",
            'value' => array(  'Yes'  => '_blank', 'No'  => '_self' ),
	        "description" => __("Select if you want to open the link in a new tab",'thefoxwp'),
		 	'dependency' =>  array( 'element' => 'style', 'value' => array('rd_cta_1','rd_cta_2'))
         ),  
		  array(
            "type" => "colorpicker",
            "class" => "",
            "heading" => __("Button text color",'thefoxwp'),
            "param_name" => "button_color",
            "value" => '', //Default Red color
            "description" => __("Choose the button text color",'thefoxwp'),
		 	'dependency' =>  array( 'element' => 'style', 'value' => array('rd_cta_1','rd_cta_2'))	
         ),   
		  array(
            "type" => "colorpicker",
            "class" => "",
            "heading" => __("Button background color",'thefoxwp'),
            "param_name" => "button_bg_color",
            "value" => '', //Default Red color
            "description" => __("Choose the button background color",'thefoxwp'),
		 	'dependency' =>  array( 'element' => 'style', 'value' => array('rd_cta_1','rd_cta_2'))	
         ),  
		  array(
            "type" => "colorpicker",
            "class" => "",
            "heading" => __("Button hover background color",'thefoxwp'),
            "param_name" => "button_hover_color",
            "value" => '', //Default Red color
            "description" => __("Choose the button hover color",'thefoxwp'),
		 	'dependency' =>  array( 'element' => 'style', 'value' => array('rd_cta_1','rd_cta_2'))		
         ), 
		 
		  array(
			"type" => "attach_image",
			"class" => "",
			"heading" => __("Upload SVG icon",'thefoxwp'),
			"param_name" => "promobox_svg_icon",
			"admin_label" => true,
			"description" => __("Upload your svg icon.",'thefoxwp'),			
		 	'dependency' =>  array( 'element' => 'style', 'value' => array('jm_custom_style'))		
    	     ),
		  
		  array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Promo Box Link",'thefoxwp'),
			"param_name" => "promobox_link",
			"admin_label" => true,
			"description" => __("Set URL for this promo box",'thefoxwp'),			
		 	'dependency' =>  array( 'element' => 'style', 'value' => array('jm_custom_style'))		
    	     ),
		  
		 array(
			"type" => "4k_icon",
			"class" => "",
			"heading" => __("Select Icon:",'thefoxwp'),
			"param_name" => "icon",
			"admin_label" => true,
			"description" => __("Select the icon from the list.",'thefoxwp'),			
		 	'dependency' =>  array( 'element' => 'style', 'value' => array('rd_cta_1'))		
    	     ),
		 	 
		  array(
            "type" => "colorpicker",
            "class" => "",
            "heading" => __("icon color",'thefoxwp'),
            "param_name" => "icon_color",
            "value" => '', //Default Red color
            "description" => __("Choose the Icon color",'thefoxwp'),
		 	'dependency' =>  array( 'element' => 'style', 'value' => array('rd_cta_1'))	
         ),		  
		  array(
            "type" => "colorpicker",
            "class" => "",
            "heading" => __("icon background color",'thefoxwp'),
            "param_name" => "icon_bg_color",
            "value" => '', //Default Red color
            "description" => __("Choose the Icon background color",'thefoxwp'),
		 	'dependency' =>  array( 'element' => 'style', 'value' => array('rd_cta_1'))	
         ),		  		 
		 array(
            "type" => "textfield",
            "class" => "",
            "heading" => __("Icon size",'thefoxwp'),
            "param_name" => "icon_size",
            "value" => __("25",'thefoxwp'),
            "description" => __("Choose the icon size (e.g 35 )",'thefoxwp'),
		 	'dependency' =>  array( 'element' => 'style', 'value' => array('rd_cta_1'))		
         ), 		  		 
		 array(
            "type" => "textfield",
            "class" => "",
            "heading" => __("Top Margin",'thefoxwp'),
            "param_name" => "margin_top",
            "value" => "0",
            "description" => __("Top margin for the Box (e.g 20 )",'thefoxwp')
         ), 
		 array(
            "type" => "textfield",
            "class" => "",
            "heading" => __("Bottom Margin",'thefoxwp'),
            "param_name" => "margin_bottom",
            "value" => "0",
            "description" => __("Bottom margin for the Box (e.g 20 )",'thefoxwp')
         ), 	 
        
      )
   ) );
 }


//if (function_exists('rd_cta')) {

function rd_cta( $atts, $content = null ) {
	extract(shortcode_atts(array(

		'style'   => 'rd_cta_1',
		'title'   => 'Box Title',
		'title_color'   => 'ea2f2f',
		'text_color'   => '',
		'bg_color'   => '',
		'border_color'   => '',
		'left_border_color'   => '',
		'button_text'   => '',
		'button_link'   => '',
		'button_color'   => '',
		'button_bg_color'   => '',
		'button_hover_color'   => '',
		'icon'   => '',
		'icon_color'   => '',
		'icon_bg_color'   => '',
		'icon_size'   => '25',
		'animation'   => '',
		'target'   => '_blank',
		'margin_top'   => '0',
		'margin_bottom'   => '0',
		'promobox_svg_icon' => '',
		'promobox_link' => ''

    ), $atts));

ob_start();
$id = RandomString(20);		

$output ='<style type="text/css" >#promo_'.$id.' {margin:'.$margin_top.'px 0 '.$margin_bottom.'px 0;}</style>';



if($style == 'jm_custom_style') {

	$img_id = preg_replace( '/[^\d]/', '', $promobox_svg_icon );

	$img2 = wp_get_attachment_image_src($img_id, 'full');
	$promobox_svgWidth = ($promobox_svg_icon) ? $promobox_svg_icon : '40px';
	
	
	$output .='<style type="text/css" >#promo_'.$id.' {color:'.$text_color.'; background:'.$bg_color.'; border:1px solid '.$border_color.';}#promo_'.$id.' .promo_title{color:'.$title_color.';}#promo_'.$id.' .promo_btn{color:'.$button_color.'; background:'.$button_bg_color.';}#promo_'.$id.' .promo_btn:hover{background:'.$button_hover_color.';}#promo_'.$id.' i{color:'.$icon_color.'; background:'.$icon_bg_color.'; font-size:'.$icon_size.'px;}</style>';

   $output .='<a class="" style="" href="'.$promobox_link.'">';
   $output .='<div id="promo_'.$id.'" class="rd_promo_box '.$style.' '.$animation.'">';
   $output .='<span class="pb_svg_icon"><img src="'.$img2[0].'"></span>';
   $output .='<div class="promo_text"><h2 class="promo_title">'.$title.'</h2><p class="cta_mt">'.$content. '<p></div>';
   $output .='<div class="pb_link_arrow"><i class="fa-angle-right"></i></div>';
   $output .='</div>';
   $output .='</a>';
   
}
	

if($style == 'rd_cta_1') {

// Enqueue the CSS
		if ( ! empty( $atts['icon'] ) ) {
            // Don't load the CSS files to trim loading time, include the specific styles via PHP
            // wp_enqueue_style( '4k-icon-' . $cssFile, plugins_url( 'icons/css/' . $cssFile . '.css', __FILE__ ) );
			$cssFile = substr( $atts['icon'], 0, stripos( $atts['icon'], '-' ) );
			wp_enqueue_style( '4k-icons',  RD_DIRECTORY . '/includes/4k-icons/css/icon-styles.css' , null, VERSION_GAMBIT_VC_4K_ICONS );
			wp_enqueue_script( '4k-icons',  RD_DIRECTORY . '/includes/4k-icons/js/script-ck.js', array( 'jquery' ), VERSION_GAMBIT_VC_4K_ICONS, true );
		}
  global $iconContents;

        include('icon-contents.php' );

		// Normal styles used for everything
        $cssFile = substr( $atts['icon'], 0, stripos( $atts['icon'], '-' ) );

        $iconFile =  RD_DIRECTORY . '/includes/4k-icons/icons/fonts/' . $cssFile;
		$iconFile = apply_filters( '4k_icon_font_pack_path', $iconFile, $cssFile );

		// Fix ligature icons (these are icons that use more than 1 symbol e.g. mono social icons)
		$ligatureStyle = '';
        if ( $cssFile == 'mn' ) {
            $ligatureStyle = '-webkit-font-feature-settings:"liga","dlig";-moz-font-feature-settings:"liga=1, dlig=1";-moz-font-feature-settings:"liga","dlig";-ms-font-feature-settings:"liga","dlig";-o-font-feature-settings:"liga","dlig";
                         	 font-feature-settings:"liga","dlig";
                        	 text-rendering:optimizeLegibility;';
        }

		$iconCode = '';
		if ( ! empty( $atts['icon'] ) ) {
			$iconCode = $iconContents[ $atts['icon'] ];
		}

		$ret = "<style type='text/css'>
            @font-face {
            	font-family: '" . $cssFile . "';
            	src:url('" . $iconFile . ".eot');
            	src:url('" . $iconFile . ".eot?#iefix') format('embedded-opentype'),
            		url('" . $iconFile . ".woff') format('woff'),
            		url('" . $iconFile . ".ttf') format('truetype'),
            		url('" . $iconFile . ".svg#oi') format('svg');
            	font-weight: normal;
            	font-style: normal;
            }
            #promo_".$id." ." . $atts['icon'] . ":before { font-family: '" . $cssFile . "'; font-weight: normal; font-style: normal; }
            /* #promo_".$id." .". $atts['icon'] . ":before { content: \"" . $iconCode . "\"; $ligatureStyle } */
";

		$ret .= "</style>";

		// Compress styles a bit for readability
		$ret = preg_replace( "/\s?(\{|\})\s?/", "$1",
			preg_replace( "/\s+/", " ",
			str_replace( "\n", "", $ret ) ) )
			. "\n";


	$output .= $ret;	
	
	$output .='<style type="text/css" >#promo_'.$id.' {color:'.$text_color.'; background:'.$bg_color.'; border:1px solid '.$border_color.';}#promo_'.$id.' .promo_title{color:'.$title_color.';}#promo_'.$id.' .promo_btn{color:'.$button_color.'; background:'.$button_bg_color.';}#promo_'.$id.' .promo_btn:hover{background:'.$button_hover_color.';}#promo_'.$id.' i{color:'.$icon_color.'; background:'.$icon_bg_color.'; font-size:'.$icon_size.'px;}</style>';



   $output .='<div id="promo_'.$id.'" class="rd_promo_box '.$style.' '.$animation.'">';
   $output .='<i class="'.$icon.'"></i>';
   $output .='<div class="promo_text"><h2 class="promo_title">'.$title.'</h2><p class="cta_mt">'.$content. '<p></div>';
   $output .='<a class="promo_btn" href="'.$button_link.'" target="'.$target.'" >'.$button_text.'</a></div>';
   
}


if($style == 'rd_cta_2') {


	
	$output .='<style type="text/css" >#promo_'.$id.' {color:'.$text_color.'; background:'.$bg_color.'; border:1px solid '.$border_color.';}#promo_'.$id.' .promo_text{border-left:10px solid '.$left_border_color.'; }#promo_'.$id.' .promo_title{color:'.$title_color.';}#promo_'.$id.' .promo_btn{color:'.$button_color.'; background:'.$button_bg_color.';}#promo_'.$id.' .promo_btn:hover{background:'.$button_hover_color.';}</style>';



   $output .='<div id="promo_'.$id.'" class="rd_promo_box '.$style.' '.$animation.'">';
   $output .='<div class="promo_text"><div class="promo_text_ctn"><h2 class="promo_title">'.$title.'</h2><p class="cta_mt">'.$content. '<p></div>';
   $output .='<a class="promo_btn" href="'.$button_link.'" target="'.$target.'" >'.$button_text.'</a></div></div>';
   
}


if($style == 'rd_cta_3') {


	
	$output .='<style type="text/css" >#promo_'.$id.' {color:'.$text_color.'; background:'.$bg_color.';}#promo_'.$id.' .promo_text{border:5px solid '.$border_color.'; }#promo_'.$id.' .promo_title{color:'.$title_color.';}</style>';



   $output .='<div id="promo_'.$id.'" class="rd_promo_box '.$style.' '.$animation.'">';
   $output .='<div class="promo_text"><h2 class="promo_title">'.$title.'</h2><p class="cta_mt">'.$content. '<p></div>';
   $output .='</div>';
   
}


echo !empty( $output ) ? $output : '';

$output_string = ob_get_contents();
ob_end_clean();
return $output_string;

}
add_shortcode('rd_cta', 'rd_cta');
	
//}

// add option to hide gravityform labels
add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );




/**function gruntify_gfrom_stay($form) {

    global $post;
	$formdiv = '/#gform_wrapper_1';
    // Get current page url
    $current_page_url = get_post_permalink( $post->ID );
	
    if ( array_key_exists( 'confirmations' , $form ) ) {
		
        foreach ( $form[ 'confirmations' ] as $key => $confirmation ) {

            $form[ 'confirmations' ][ $key ][ 'type' ] = 'redirect';
            $form[ 'confirmations' ][ $key ][ 'message' ] = 'test message 1';
            $form[ 'confirmations' ][ $key ][ 'url' ] = $current_page_url.$formdiv;
            $form[ 'confirmations' ][ $key ][ 'queryString' ] = 'message=success';
        }

    }

    if ( array_key_exists( 'confirmation' , $form ) ) {

        $form[ 'confirmation' ][ 'type' ] = 'redirect';
        $form[ 'confirmation' ][ 'message' ] = 'test message 2';
        $form[ 'confirmation' ][ 'url' ] = $current_page_url;
        $form[ 'confirmation' ][ 'queryString' ] = 'message=success';

    }
	
    return $form;

}
add_filter( 'gform_pre_submission_filter' , "gruntify_gfrom_stay" , 10 , 1 );

add_action( 'gform_after_submission', 'foo_', 10, 2 );
function foo_( $notifications, $form ) {
    $message = '<p style="font-size:14px;text-align:center;padding:5px">Thanks for contacting us! We will get in touch with you shortly.</p>';
    ?>
	<script>
		jQuery('#gform_1').append('<p><?php echo $message; ?></p>');
		jQuery('.gform_ajax_spinner').hide();
	</script>
<?php
} 
**/


add_filter( 'gform_ajax_spinner_url', 'spinner_url' );
	function spinner_url( $src ) {
		// return 'http://www.gruntify.com/wp-content/uploads/2017/08/ajax-loader.gif';
		return get_stylesheet_directory_uri() . '/images/ajax-loader.gif';
	}



///////////////////////////////////////////////////////////////////////////////////////
//////                                                                           /////
/////               ADD STAFF MEMBER TESTIMONIALS SHORTCODES                    /////
////                                                                           /////
///////////////////////////////////////////////////////////////////////////////////

add_action( 'vc_before_init', 'rd_testimonials_sc2' );
function rd_testimonials_sc2() {
vc_map( array(
    "name" => __("Staff Member Testimonials", 'thefoxwp'),
    "base" => "testimonials_ctn2",
	"weight" => "76",
	"icon"		=> get_template_directory_uri() . "/images/vc_icons/vc_testimonials.png",
    "as_parent" => array('only' => 'testimonial_sc2'),
    "content_element" => true,
    "show_settings_on_create" => true,
    "params" => array(
	       array(
            "type" => "dropdown",
            "class" => "",
            "heading" => __("Testionials Style",'thefoxwp'),
            "param_name" => "style",
            "value" => array ("Style custom by Metamode" => "custom-style",),
            "description" => __("Select the Testimonials Style ( design )",'thefoxwp')
         ),
    ),
    "js_view" => 'VcColumnView'
) );


vc_map( array(
	'name' => __( 'Staff Member Testimonial', 'thefoxwp' ),
	'base' => 'testimonial_sc2',
	"icon"		=> get_template_directory_uri() . "/images/vc_icons/vc_testimonials.png",
	'category' => __( 'Content', 'thefoxwp' ),
	  "as_child" => array('only' => 'testimonials_ctn2'),
	'params' => array(

		array(
			'type' => 'attach_image',
			'heading' => __( 'Staff member image', 'thefoxwp' ),
			'param_name' => 'staff_image',
			'value' => '',
			'description' => __( 'Select the image for the staff member (required).', 'thefoxwp' )
		),	
		array(
			'type' => 'attach_image',
			'heading' => __( 'Staff member slider background image', 'thefoxwp' ),
			'param_name' => 'staff_image_bg',
			'value' => '',
			'description' => __( 'Select the slider background image for the staff member(required).', 'thefoxwp' )
		),
		array(
			'type' => 'textfield',
			'heading' => __( 'Staff member name', 'thefoxwp' ),
			'param_name' => 'member',
			'description' => __( 'Enter Staff member name.', 'thefoxwp' )
		),
		array(
			'type' => 'textfield',
			'heading' => __( 'Staff member information', 'thefoxwp' ),
			'param_name' => 'a_info',
			'description' => __( 'Enter staff member information.', 'thefoxwp' )
		),
		array(
			'type' => 'textarea_html',
			//holder' => 'div',
			//'admin_label' => true,
			'heading' => __( 'Quote from author', 'thefoxwp' ),
			'param_name' => 'content',
			'value' => __( 'I am promo text. Click edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.', 'thefoxwp' )
		),

    ),
) );
if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
    class WPBakeryShortCode_testimonials_ctn2 extends WPBakeryShortCodesContainer {
    }
}
if ( class_exists( 'WPBakeryShortCode' ) ) {
    class WPBakeryShortCode_testimonial_sc2 extends WPBakeryShortCode {
    }
}
}


if (!function_exists('testimonials_ctn2')) {


function testimonials_ctn2( $atts, $content ){

extract( shortcode_atts( array(
        'style' => 'custom-style',
		'staff_image'   => '',
	), $atts ) );
ob_start();


global $rd_data;

if($t_color == '' ){
	$t_color = $rd_data['rd_content_text_color'];
}
if($h_color == '' ){
	$h_color = $rd_data['rd_content_heading_color'];
}
if($hl_color == '' ){
	$hl_color = $rd_data['rd_content_hl_color'];
}
if($bg_color == '' ){
	$bg_color = $rd_data['rd_content_bg_color'];
}
if($b_color == '' ){
	$b_color = $rd_data['rd_content_border_color'];
}

$id = RandomString(20);	



$output= '<style type="text/css" >';


if($style == 'custom-style' ){

	$output.= '#tm_'.$id.'.custom-style .tm_author,#tm_'.$id.'.custom-style .tm_info{ color:'.$h_color.';}#tm_'.$id.'.custom-style .tm_text{ color:'.$t_color.';}#tm_'.$id.'.rd_tm_1 .rd_tm_pager a{background:'.$t_color.';}#tm_'.$id.'.rd_tm_1 .rd_tm_pager a.selected{background:none!important; border:1px solid '.$t_color.'; }';
	
}


$output.= '#tm_'.$id.' {margin-top:'.$margin_top.'px; margin-bottom:'.$margin_bottom.'px;}</style>';


$output.= '<script>
		jQuery(window).load(function(){
			jQuery(".bxslider").bxSlider({
				pagerCustom: "#bx-pager",
				infiniteLoop: false,
				controls: false,
				responsive: true
			});
		});
		jQuery(document).ready(function() {
        jQuery(".bxslider li img.mem").each(function(i) {
        jQuery(this).addClass("item" + (i+1));
		jQuery(".overlay_").show();
        });
		jQuery(".s1").append(jQuery(".item1").clone());
		jQuery(".s2").append(jQuery(".item2").clone());
		jQuery(".s3").append(jQuery(".item3").clone());
				
      });</script>
	  <style>
	  .pagi{
	  	border: 4px solid #ffffff;
		height: 88px;
		width: 88px;
		display: inline-flex;
		border-radius: 50%;
		overflow: hidden;
	  }
	  .pagi img{
	  	width: 80px;
		height: 80px;
	  }
	  #bx-pager{
		  text-align: center;
		  position: relative;
		  top: 2em;
		  z-index: 1
	  }
	   .opac{
			background: #fff;
			width: 86px;
			height: 85px;
			position: absolute;
			opacity: .6;
			margin-left: -3px;
			border: 4px solid #fff;
			border-radius: 50%;
	   }
	   .active .opac{
	   		display: none;
			transition: all 0.6s linear
	   }
	   .item-bg-wrap {
	   		display: flex;
	   		position: relative;
	   }
	  .item-bg-wrap img {
	  		display: block;
	  }
	  .img-overlay {
	  		background: rgba(26,35,39,.8);
	  		position: absolute;
	  		bottom: 0;
	  		left: 0;
	  		right: 0;
	  		top: 0;
	  		z-index: 25;
	  }
	  .box-wrapper{
	     width: 100%!important;
	  }
	   .copy_ {
			color: #fff;
			left: 50%;
			letter-spacing: normal;
			margin: 0 auto;
			position: absolute;
	   		text-align: center;
		    top: 50%;
		    transform: translateY(-50%);
		    z-index: 50;
	   }
	   .copy_ .testimony {
			font-size: 30px;
			line-height: 1.4em;
	   		font-weight: 300;
	   		margin-bottom: 1em;
	   }
	   .copy_ .cite {
	   		font-size: 16px;
	   		font-weight: 700;
	   		text-transform: uppercase;
	   }
	   .copy_ .cite .job-title {
	   		color: #00b3ff;
	   }
		@media print, screen and (min-width: 75em) {
		   	.copy_{
		   		margin-left: -500px;
				width: 1000px;
			}
		}
	    @media screen and (min-width: 64em) and (max-width: 74.9375em) {
		   	.copy_{
		   		margin-left: -350px;
				width: 700px;
			}
		}
		@media screen and (max-width: 63.9375em) {
		   	.copy_{
		   		margin-left: -300px;
				width: 600px;
			}
		   .copy_ .testimony {
				font-size: 24px;
				padding: 0 1em;
		   }
		   .copy_ .cite {
		   		font-size: 14px;
		   }
		}
		@media screen and (max-width: 39.9375em) {
			.item-bg-wrap {
		   		display: block;
		   		height: 400px;
		   		text-align: center;
			}
			.item-bg-wrap img {
				display: none;
			}
			.copy_ {	
				left: auto;
				margin-left: 0;
				margin:0 auto;
				width:100%;
			}
		    .copy_ .testimony {
				font-size: 20px !important;
		    }
			.copy_ .cite {
					font-size: 14px !important;
			}
		}			
		@media screen and (max-width: 280px) {

		}	
	  </style>';
$output.= '
<div class="rd_testimonials_ctn">
<div id="bx-pager">
  <a data-slide-index="0" href="" class="pagi s1"><div class="opac"></div></a>
  <a data-slide-index="1" href="" class="pagi s2"><div class="opac"></div></a>
  <a data-slide-index="2" href="" class="pagi s3"><div class="opac"></div></a>
</div>
</div>
<ul class="bxslider">'.do_shortcode(strip_tags($content)).'</ul>'
;

echo !empty( $output ) ? $output : '';


$output_string = ob_get_contents();
ob_end_clean();
return $output_string; 

}
add_shortcode( 'testimonials_ctn2', 'testimonials_ctn2' );
}



if (!function_exists('testimonial_sc2')) {
	function testimonial_sc2( $atts, $content = null ) {
		$src = get_stylesheet_directory_uri();
		extract(shortcode_atts(array(
			'staff_image'   => '',
			'staff_image_bg'   => '',
			'member' => 'Enter Staff member name.',
			'a_info' => 'Enter Staff information.',
	    ), $atts));
	    $site_url = get_site_url();
		ob_start();
		$id = preg_replace( '/[^\d]/', '', $staff_image );
		$img_id_bg = preg_replace( '/[^\d]/', '', $staff_image_bg );
		// $img = wpb_getImageBySize( array( 'attach_id' => $staff_image, 'thumb_size' => '250'  ) );
		// $img_member = wp_get_attachment_image_src($id, 'full');
		$img_url = wp_get_attachment_url( $id );
		$img_bg_url = wp_get_attachment_url( $img_id_bg );
		//echo '<pre>' . print_r($shit_id,1) . '</pre>';
		//$img_member_bg = wp_get_attachment_image_src($img_id_bg, 'full');
		$copy = $content;
		$bg_url = $site_url . $img_bg_url;
		echo '<li>
			<div class="item-bg-wrap" style="background:url('. $bg_url .') top center no-repeat;background-size:cover;">
				<div class="img-overlay"></div>
				<img src="' . get_stylesheet_directory_uri() . '/images/1920x700.png">
				<div class="copy_">
					<div class="testimony">' . $copy .'</div>
					<div class="cite">
						<p class="member">'.$member.', <span class="job-title">'.$a_info.'</span></p>
					</div>
				</div>
				
			</div>
			<div style="display:none"><img class="mem" src="'. $site_url . $img_url. '"/></div>
		</li>';
		return ob_get_clean();
	}
	add_shortcode('testimonial_sc2', 'testimonial_sc2');
}



///////////////////////////////////////////////////////////////////////////////////////
//////                                                                           /////
/////                     CPT IMAGE GALLERY SLIDER                              /////
////                                                                           /////
///////////////////////////////////////////////////////////////////////////////////
function cpt_image_slider() {
  $labels = array(
    'name'               => _x( 'Slider', 'post type general name' ),
    'singular_name'      => _x( 'Slider', 'post type singular name' ),
    'add_new'            => _x( 'Add New', 'Slider' ),
    'add_new_item'       => __( 'Add New Slider' ),
    'edit_item'          => __( 'Edit Slider' ),
    'new_item'           => __( 'New Slider' ),
    'all_items'          => __( 'All Slider' ),
    'view_item'          => __( 'View Slider' ),
    'search_items'       => __( 'Search Slider' ),
    'not_found'          => __( 'No Slider found' ),
    'not_found_in_trash' => __( 'No Slider found in the Trash' ), 
    'parent_item_colon'  => '',
    'menu_name'          => 'Image Slider'
  );
  $args = array(
    'labels'        => $labels,
    // 'description'   => 'Holds our artists and artist specific data',
    'public'        => true,
    'supports'      => array( 'title', 'thumbnail', 'excerpt' ),
    'has_archive'   => true,
  );
  register_post_type( 'image-slider', $args ); 
}
add_action( 'init', 'cpt_image_slider' );


// shortcode to show image slider
function cpt_metamode_slider_shortcode(){
        $args = array(
            'post_type' => 'image-slider',
            'post_status' => 'publish'
        );
		?>
	<section class="center slider">
		<?php

        $grid = '';
        $query = new WP_Query( $args );
        if( $query->have_posts() ){
            while( $query->have_posts() ){
                      $query->the_post();?>
				
	<?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
		<div>
		  <img src="<?php echo $url;?>" alt="slider image">
		</div>
<?php			
            }
        }
         wp_reset_postdata();
       ?>
	</section><!-- end .center .slider  -->
<style>

    .slick-slide img {
      width: 100%;
    }
    .slick-prev:before,
    .slick-next:before {
	  color: #0096D6;
	  font-family: 'fontawesome';
	  font-size: 30px!important;
    }
	.slick-prev,.slick-next{
		width: 60px;
		height: 60px;
		background-color: #ffffff;
		border-radius: 50%;
		box-shadow: 0 0 20px 0 rgba(0,0,0,0.3);
	}
	.slick-prev:hover,.slick-next:hover{
		background-color: #ffffff;
	}
	.slick-prev:focus,.slick-next:focus{
		background-color: #ffffff;
	}
	.slick-prev:before{
		content:"\f104";
		position: relative;
		left: -2px;
		top: -1px;
	}
	.slick-next:before{
		content:"\f105";
		position: relative;
		top: -1px;
		left: 2px;
	}
	.slick-dots{
		top: 100%;
		margin-top: 1em;
	}
	.slick-dots li.slick-active button:before{
		color: #0096D6;
		font-size: 12px;
	}
	.slick-dots li.slick-active{
		opacity: unset;
	}
	.slick-dots li button:before{
		color: #d8d8d8;
		font-size: 12px;
		opacity: unset;
	}
    .slick-slide {
      transition: all ease-in-out .3s;
      opacity: .2;
    }
    .slick-active {
      opacity: .5;
    }
    .slick-current {
      opacity: 1;
    }
	.slick-list{
		  overflow: unset;
	}
	.slick-arrow{
		z-index: 1;
	}
	.slick-dots li{
		margin: 0;
	}
	.slick-prev{
		left: -47%!important;
	}
	.slick-next{
		right: -47%;
	}
	*:focus {
    outline: none;
}
@media print, screen and (min-width: 64em) {
	.slider {
      width: 50%;
      margin: 100px auto;
    }
    .slick-slide {
      margin: 0px 20px;
    }
}
</style>
<script type="text/javascript">
    jQuery(document).on('ready', function() {
      jQuery(".center").slick({
        dots: true,
        infinite: true,
        centerMode: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 640,
                settings: {
                    centerMode: false
                }
            }		            
        ]
      });
    });
</script>
	 <?php
    }
add_shortcode( 'metamode-image-slider', 'cpt_metamode_slider_shortcode' );
include( 'jobs/jobs.php' );

function popup_form( $atts ) {
		$atts = shortcode_atts( [
				'gform_id' => 1,
				'text' => __( 'Open Form', 'gruntify' ),
				'btn_class' => 'btn white'

		], $atts, 'popup_form' );
		$gform_id = absint( $atts['gform_id'] );
		global $gform_ids;
		if( empty( $gform_ids ) ) {
			$gform_ids = [
				$gform_id => $gform_id
			];
		}
		else {
			$gform_ids[$gform_id] = $gform_id;
		}
		wp_enqueue_style( 'simple-modal', get_stylesheet_directory_uri() . '/css/simple-modal.min.css' );
		wp_enqueue_script( 'simple-modal', get_stylesheet_directory_uri() . '/js/jquery.simple-modal.js', [ 'jquery' ] );
		wp_enqueue_script( 'popup-form', get_stylesheet_directory_uri() . '/js/popup-form.js', [ 'simple-modal' ] );
		add_action( 'wp_footer', 'add_dialog', 10, 1 );
        return '<a class="' . $atts['btn_class'] . ' open-dialog" href="#" data-form_id="' . $atts['gform_id'] . '" rel="nofollow">' . $atts['text'] . '</a>';
}
add_shortcode( 'popup_form', 'popup_form' );

function add_dialog( $form_id ) {
		global $gform_ids;
		if( empty( $gform_ids ) ) return;
		foreach( $gform_ids as $gform_id ): 
			$gform = GFAPI::get_form( $gform_id ); 
			if( empty( $gform ) ) continue; ?>
			<div id="gform-<?php echo $gform_id; ?>" class="modal">
				<div class="modal__content">
					<div class="modal__header">
						<?php //echo $gform['title']; ?>
						<a class="modal__close" href="#0" rel="nofollow">
							<span class="fa fa-times"></span>
						</a>
					</div>
					<?php echo GFForms::get_form( $gform_id, true, false, false, [], true ); ?>
				</div>
			</div>
		<?php endforeach;
}

function custom_product_menu() {
		wp_enqueue_script( 'timeline2', get_stylesheet_directory_uri() . '/js/product-menu.js', array( 'jquery' ), '', true ); ?>
		<style>
		.nav-region > div {
		    z-index: 1 !important;
		}
		#product-menu-section {
			border-bottom: 1px solid #e0e0e0;
			/*padding: 20px 0;*/
		}
		#product-menu-section.fixed-me .sticky-nav {
			background: #fff;
			border-bottom: 1px solid #e0e0e0;
			position: fixed;
			left: 0;
			right: 0;
			top: 0;
			width: 100%;
		}
		#nav-container {
			margin: 0 auto;
			max-width: 940px;
		}
		/*#product-menu {*/
		/*	display: flex;*/
		/*}*/
		/*#product-menu li {*/
		/*	display: inline-block;*/
		/*	width: 100%;*/
		/*}		*/
		#product-menu li a {
			color: #999;
			display: inline-block;
			border-bottom: 3px solid transparent;
			border-top: 3px solid transparent;
			height: 84px;
			line-height: 78px;
			/*padding: 15px 0;*/
			text-align: center;
			width: 100%;
		}
		#product-menu li:hover a {
			color: #00a9ff;
		}
		#product-menu li a:hover,
		#product-menu li a.active {
			color: #00a9ff;
			border-bottom: 3px solid #00a9ff;
		}
		@media (max-width: 1000px) {
			#product-menu-section {
				display: none;
			}
		}
		</style>
		<div class="sticky-nav">
			<!--<nav id="nav-container">-->
			<!--	<ul id="product-menu">-->
			<!--	    <li>-->
			<!--	    	<a href="#plan-build" tabindex="0"><span>Plan &amp; Build</span></a>-->
			<!--	    </li>-->
			<!--	    <li>-->
			<!--	    	<a href="#collect" tabindex="0"><span>Collect &amp; Report</span></a>-->
			<!--	    </li>-->
			<!--	    <li>-->
			<!--	    	<a href="#map" tabindex="0"><span>Map &amp; Track</span></a>-->
			<!--	    </li>-->
			<!--	    <li>-->
			<!--	    	<a href="#action" tabindex="0"><span>Action &amp; Analyze</span></a>-->
			<!--	    </li>-->
				    <!--<li>-->
				    <!--	<a href="#more-features" tabindex="0"><span>More Features</span></a>-->
				    <!--</li>-->
			<!--	</ul>-->
			<!--</nav>-->
			<?php wp_nav_menu( [
			    'menu' => 292,
			    'container' => 'nav',
			    'container_id' => 'nav-container',
			    'menu_id' => 'product-menu'
			] ); ?>
		</div>
		<?php
}
add_shortcode( 'custom_product_menu', 'custom_product_menu' );


function twentytwenty() {
	wp_enqueue_style( 'twentytwenty',  get_stylesheet_directory_uri() . '/css/twentytwenty.css');
	wp_enqueue_script( 'jquery-event', get_stylesheet_directory_uri() . '/js/jquery.event.move.js', array( 'jquery' ) );
	wp_enqueue_script( 'jquery-twentytwenty', get_stylesheet_directory_uri() . '/js/jquery.twentytwenty.js', array( 'jquery-event' ) );
	wp_enqueue_script( 'twentytwenty', get_stylesheet_directory_uri() . '/js/twentytwenty.js', array( 'jquery-twentytwenty' ) );
}
add_shortcode( 'twentytwenty', 'twentytwenty' );


/* Forced use of Gruntify logo on all user profile photos */
add_filter( 'get_avatar', 'custom_avatar', 10, 5 );
function custom_avatar( $avatar = '', $id_or_email, $size = 96, $default = '', $alt = '' ) {
		return '<img src="https://www.gruntify.com/wp-content/uploads/2018/05/logomark-positive-103x103.png" alt="Gruntify" class="avatar wp-user-avatar wp-user-avatar-103 photo avatar-default" width="103" height="103">';
}

function custom_before_header() {
    ?>
<div class="ph-item">
    <div class="ph-col-12">
        <div class="ph-picture"></div>
        <div class="ph-row">
            <div class="ph-col-6 big"></div>
            <div class="ph-col-4 empty big"></div>
            <div class="ph-col-2 big"></div>
            <div class="ph-col-4"></div>
            <div class="ph-col-8 empty"></div>
            <div class="ph-col-6"></div>
            <div class="ph-col-6 empty"></div>
            <div class="ph-col-12"></div>
        </div>
    </div>
</div>
    <?php
}
//add_action( '__before_header', 'custom_before_header' );









add_shortcode( 'price_slider', 'price_slider' );
function price_slider() {
        wp_enqueue_style( 'jquery-ui-slider', get_stylesheet_directory_uri() . '/css/jquery-ui-1.12.1/jquery-ui.min.css' );
        wp_enqueue_style( 'jquery-ui-lightness', get_stylesheet_directory_uri() . '/css/jquery-ui-1.12.1/jquery-ui.theme.min.css' );
        wp_enqueue_style( 'price-slider', get_stylesheet_directory_uri() . '/css/price-slider.css' );
        wp_enqueue_script( 'jquery-ui-slider' );
        wp_enqueue_script( 'jquery-ui-selectmenu' );
        wp_enqueue_script( 'price-slider', get_stylesheet_directory_uri() . '/js/price-slider.js', [ 'jquery-ui-slider' ], '0.1' );
        wp_enqueue_script( 'touch-punch', get_stylesheet_directory_uri() . '/js/jquery.ui.touch-punch.min.js', [ 'jquery-ui-slider' ], '0.2.3' );

        ob_start(); ?>
        <div class="range-wrap">
            <div id="price-slider" class="price-slider"></div>
            <span id="min-value"class="min"></span>
            <span id="max-value" class="max"></span>
        </div>
        <?php return ob_get_clean();
}












function custom_menu_enqueues() {
            wp_enqueue_style( 'custom-menu', get_stylesheet_directory_uri() . '/css/custom-menu.css?ver=1.3' . time() );
            wp_enqueue_script( 'custom-menu', get_stylesheet_directory_uri() . '/js/custom-menu.js?ver=1.3' . time(), [ 'jquery' ], '0.1' );
}
add_action( 'wp_enqueue_scripts', 'custom_menu_enqueues' );