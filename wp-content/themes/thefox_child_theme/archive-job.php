<?php
wp_enqueue_style( 'xygrid', get_stylesheet_directory_uri() . '/jobs/xygrid.css', [], '6.4.3' );
wp_enqueue_style( 'jobs', get_stylesheet_directory_uri() . '/jobs/jobs.css', [], '6.4.3' );
get_header(); ?>
<script type="text/javascript" charset="utf-8">
		var j$ = jQuery;
		j$.noConflict();
		"use strict";
		j$('#header_container').css('position', 'absolute');
		j$('#header_container').css('width', '100%');	
		j$('header').addClass('transparent_header');		
		j$('.header_bottom_nav').addClass('transparent_header');
		
</script>
<div class="job-header section-gradient">
        <div class="wrapper section_wrapper">
                <h1 class="job-title"><?php _e( 'Jobs', THEME_NAME ); ?></h1>

        </div>
</div>
<section class="section jobs">
		<div class="grid-container">
				<div class="grid-x grid-margin-x align-center">
						<div class="large-8 cell">
								<div class="section-title-wrap">
									<h2 class="section-title">Current Job Openings</h2>
									<p>Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor</p>
								</div>
						</div>
				</div>
				<div class="grid-x grid-margin-x small-up-1 large-up-3 align-center">
		        <?php if ( have_posts() ):
		                while ( have_posts() ) : the_post();
		                		$post_id = get_the_ID();
				                $terms = jobs::get_job_cats( $post->ID );
				                $job_status = get_post_meta( $post_id, 'job_status', true );  ?>
		    			<div class="cell">
		    					<div class="job-card">
		    							<div class="job-title-wrap">
				            					<h3>
				            							<a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a>
				            					</h3>
				            					<h5 class="job-status"><?php echo jobs::job_status_alt( $job_status ); ?></h5>
		            					</div>
		            					<?php if( $terms ):
												echo implode( $terms, ',' );
		            					endif; ?>
		            			</div>
		        		</div>
		                <?php endwhile; ?>
						<div class="navigation clearfix">
						<?php kriesi_pagination(); ?>
						</div>
		        <?php else: ?>

		        <?php endif; ?>
				</div>
		</div>
</section>
<?php get_footer();