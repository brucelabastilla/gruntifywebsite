jQuery(document).ready(function() {
    
    var pricing = { 'free': 0, 'pro': 29 },
    pricingYearly = { 'free': 0, 'pro': 22 },
    updateSwitch = function() {
        var packagePeriod = jQuery('#package-period').val();
        if(packagePeriod=='monthly') {
           jQuery('#switcher,#pricing-section,#heading-section').addClass('is-yearly');
           //jQuery('#period').html('yr');
           jQuery('#period').html('mo');
           packagePeriod = 'yearly';
        }
        else {
           jQuery('#switcher,#pricing-section,#heading-section').removeClass('is-yearly');
           jQuery('#period').html('mo');
           packagePeriod = 'monthly';
        }
        jQuery('#period2').html(packagePeriod);
        jQuery('#package-period').val(packagePeriod);
    },
    updatePrice = function(seats) {
        var packagePrice = pricing[jQuery('#package').val()];
        // noMonths = (jQuery('#package-period').val()=='monthly')? 1: 12;
        if(jQuery('#package-period').val()=='yearly') {
            packagePrice = pricingYearly[jQuery('#package').val()];
        }
        jQuery('#seats').html(seats);
        jQuery('#calculated-price').html(commaFormat(seats*packagePrice));
        
        /* 19 and 25 below is pro price annually and monthly */
        var savings = (seats*29)-(seats*22);
        jQuery('#savings').html(commaFormat(savings));
    },
    commaFormat = function(num) {
        return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    },
    minValue = 1,
    maxValue = 100;

    jQuery('#price-slider').slider({
        range: 'max',
        min: minValue,
        max: maxValue,
        value: 50,
        slide: function(e,ui) {
            updatePrice(ui.value);
        }
    }).removeClass('ui-corner-all'); 
    
    jQuery('#min-value').html(minValue);
    jQuery('#max-value').html(maxValue);
    
    // jQuery('#package').selectmenu({
    //     disabled: true,
    //     change: function() {
    //         updatePrice(jQuery('#price-slider').slider('value'));
    //     }
    // });
    
    updateSwitch();
    updatePrice(jQuery('#price-slider').slider('value'));
    
    jQuery('#switcher').click(function(){
        updateSwitch();
        updatePrice(jQuery('#price-slider').slider('value'));
    });
    
    if(jQuery('#notification').length) {
        jQuery('#notification').show().prependTo('body');
        jQuery('#notification .close').click(function(e){
           e.preventDefault();
           jQuery('#notification').fadeOut();
        });
    }
    
});