var j$ = jQuery;

j$.noConflict();

"use strict";

j$(document).ready(function() {
//jQuery('.ph-item').remove();
  if(jQuery('.bg-image').length) {
    jQuery('.bg-image').each(function(){
      var sImg = jQuery(this).siblings('.main-image');
      //console.log(jQuery('.vc_single_image-img',prnt).length);
      if(sImg.length) {
        var mImg = jQuery('img',sImg);
        if(mImg.length) {
          var src = mImg.eq(0).attr('src');
          jQuery(this).css('background-image', 'url(' + src + ')');
        }
      }
    });
  }

	set_fixednav2();
	
    //onepage_activ_menu();
    scrollers();

    // hrg
    hrg_make_links_clikable();
	
	// request demo nav
	request_demo_nav();


    // j$('#gform_2').submit(function(e) {

    //   e.preventDefault();

    //   // Get Form ID for submission URL //
    //   var formID = j$(this).attr('id');
    //   formID = formID.replace('gform_', '');
    //   var formURL = '/gravityformsapi/forms/'+formID+'/submissions';

    //   // Get Form Input Data and Format JSON for Endpoint //
    //   var formArray = j$(this).serializeArray();
    //   var formData = [];
    //   j$.each(formArray, function(index, data) {
    //     var name = data['name'];
    //     var value = data['value'];
    //     formData[name] = value;
    //   });

    // if (formID == 1) {
    //     var firstName = j$("#input_1_1_3").val();
    //     var lastName = j$("#input_1_1_6").val();
    //     formData['input_1_3'] = firstName;
    //     formData['input_1_6'] = lastName;
    // }

    //   formData = j$.extend({}, formData);
    //   var data = { input_values : formData };

    //   // AJAX to Submit Form //
    //   j$.ajax({
    //     url: formURL,
    //     method: 'POST',
    //     data: JSON.stringify(data)
    //   }).done(function (data, textStatus, xhr) {
    //     // This is the HTML that is output as a part of the Confirmation Message //
    //     j$("#gform_"+formID).append(data.response.confirmation_message);
    //     j$(".gform_ajax_spinner").hide();
    //   });

    // });
	
});

j$(window).scroll(function() {
   // set_fixednav2();
});

function hrg_make_links_clikable() {
	var body = j$('body.home');

	if (body.length > 0) {
		j$('div.full-width-section').css({ zIndex: '0 !important' });
	} else {
		j$('div.full-width-section').css({ zIndex: '-999 !important' });
	}
}

function request_demo_nav(){
	 j$('.nav-request-demo').find('a').addClass('jm_nav');
}


function scrollers() {
    // window.location.href = window.location.href.replace( /[\?#].*|$/, "#collect" );
    // var selector = '.ubermenu-item a';
    // j$(function() {

    //     j$(selector).on('click', function() {
    //     	// add active class to a link that is clicked
    //         j$(selector).removeClass('active');
    //         j$(this).addClass('active');

    //         // hrg - scrolling effect on links
    //         if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
    //             var target = j$(this.hash);
    //             target = target.length ? target : j$('[name=' + this.hash.slice(1) + ']');

    //             if (target.length) {
    //                 j$('html,body').animate({
    //                     scrollTop: target.offset().top
    //                 }, 800);
    //                 return false;
    //             }
    //         }
    //     });

    //     // secondary sticky menu is click
    //     j$('.smoothScroll').click(function(e) {
    //     	e.preventDefault();

    //     });
    // });

}



/////////////////////////////////
//***    Sticky Header Child Theme    ***////
/////////////////////////////////
function set_fixednav2() {
    if (j$('.sticky_header').length) {
        var sticky_navigation_offset_top = j$('.sticky_header').offset().top;
    } else {

        var sticky_navigation_offset_top = 0;
		
    }

    // var inc = 0;

    var sticky_navigation = function() {
		
        var scroll_top = j$(window).scrollTop();

        //console.log('outside');

     	if (scroll_top > sticky_navigation_offset_top && j$('#fixed_body_left').length < 1) {
            var top_value = 0;

            //console.log('else');
			
			j$('.nav-request-demo').find('a').addClass('jm_nav_sticky');
			j$('.nav-request-demo').find('a').removeClass('jm_nav');
			

            // disable sticky_header in Product tour page
            if (j$('.page-id-2514').length !== 0) {

               j$("header.sticky_header").css("position", "relative");
         
  				 j$('header').addClass('pt_remove_sticky');
				j$('header').removeClass('opaque_header');
            } 
			
		
        } else {
	
				j$('.nav-request-demo').find('a').addClass('jm_nav');
				j$('.nav-request-demo').find('a').removeClass('jm_nav_sticky');

      			// j$('header').show;
			 	 j$('header').removeClass('pt_remove_sticky');
        }


    };

    sticky_navigation();

    j$(window).scroll(function() {
        sticky_navigation();
    });


}



/////////////////////////////////
//***    Product tour sticky    ***////
/////////////////////////////////

function sticky_relocate() {
    var window_top = j$(window).scrollTop();
    var div = j$('div#sticky-anchor');

    if (div.length > 0) {
	    var div_top = div.offset().top;
	    if (window_top > div_top) {
	        j$('#sticky').addClass('stick');
	        j$('#sticky').addClass('opaque_header');
	        j$('#sticky-anchor').height(j$('#sticky').outerHeight());
			
	        // hrg
	        // j$('#sticky.opaque_header').css('padding-top', '2em');
	    } else {
	        j$('#sticky').removeClass('stick');
	        j$('#sticky-anchor').height(0);

	        // hrg
	        // j$('#sticky.opaque_header').css('padding-top', '0');
	    }
    }

}

j$(function() {
    j$(window).scroll(sticky_relocate);
    sticky_relocate();
});


// Write your javascript
// (function($) {
  
	var nav = j$('nav.ubermenu .ubermenu-nav'),
      navOffset = nav.offset(),
      stickNav = j$('ul#ubermenu-nav-main-170'),
      links = stickNav.find('a');
  
  // add active class to first link
  stickNav.find('a').first().addClass('active');
  
j$(document).ready(function(){
	if (j$('.single-post').is(':visible') ){
      j$('.nav-request-demo').find('a').addClass('jm_nav_single');
      j$('header').find('a.jm_nav').addClass('sticky_nav_single');    
  } 
  

  // fixed contact form fields not clickable
   if (j$('.page-id-9').length > 0) {
       j$('.full-width-section').css("z-index","1");
   }
  
  if (j$('.error404').length > 0){
      j$('.nav-request-demo').find('a').addClass('jm_nav_error');
      j$('.nav-request-demo').find('a').removeClass('jm_nav');
  }
  
  // custom pricing table
 //  j$('.jm_pt_1').find('div.pricetable-column-inner').addClass('jm_pricetable_colinner');
 //  j$(".pt_price:empty").append("<div class='circle_wicon'><img src='http://gruntify.com/wp-content/uploads/vector/enterprise-crown.svg'></div>");
 //  j$('.pt_price').prepend("<span class='dollar'>$</span>");
	// j$('.pricetable-name').append("<div class='bb_line'></div>");
 //  j$('.pricetable-header').append("<div class='per-user-month'>Per user / month</div>");
	// $('.price_details').hide();
  
  if ( j$('.page-id-1590').is(':visible')){
      j$('.blend_mode_multiply').append("<div class='gradient_overlay'></div>");
  }
  
  // you can add .fresco class to any anchor link on the inner pages
  if ( !j$('body').hasClass("home") ){
       j$('a.fresco').css({'background-color':"transparent"});
  }
    
});
  
// })(jQuery);



/////////////////////////////////////////////////////////
//***   JS copied from sublime custom js plugin    ***////
////////////////////////////////////////////////////////
//(function($) {
  
	var nav = j$('nav.ubermenu .ubermenu-nav'),
      navOffset = nav.offset(),
      stickNav = j$('ul#ubermenu-nav-main-170'),
      links = stickNav.find('a');
  
  // add active class to first link
  stickNav.find('a').first().addClass('active');
  
j$(document).ready(function(){
	if (j$('.single-post').is(':visible') ){
      j$('.nav-request-demo').find('a').addClass('jm_nav_single');
      j$('header').find('a.jm_nav').addClass('sticky_nav_single');    
  } 
  

  // fixed contact form fields not clickable
   if (j$('.page-id-9').length > 0) {
       j$('.full-width-section').css("z-index","1");
   }
  
  if (j$('.error404').length > 0){
      j$('.nav-request-demo').find('a').addClass('jm_nav_error');
      j$('.nav-request-demo').find('a').removeClass('jm_nav');
  }
  
  // custom pricing table
  j$('.jm_pt_1').find('div.pricetable-column-inner').addClass('jm_pricetable_colinner');
  j$(".pt_price:empty").append("<div class='circle_wicon'><img src='http://gruntify.com/wp-content/uploads/vector/enterprise-crown.svg'></div>");
  j$('.pt_price').prepend("<span class='dollar'>$</span>");
  j$('.pricetable-name').append("<div class='bb_line'></div>");
  j$('.pricetable-header').append("<div class='per-user-month'>Per user / month</div>");
	// $('.price_details').hide();
  
  if ( j$('.page-id-1590').is(':visible')){
      j$('.blend_mode_multiply').append("<div class='gradient_overlay'></div>");
  }
  
  // you can add .fresco class to any anchor link on the inner pages
  if ( !j$('body').hasClass("home") ){
       j$('a.fresco').css({'background-color':"transparent"});
  }
    
});
  
//})(jQuery);

// Fill SVG image color
jQuery(function(){
    jQuery('img.svg_playbtn').each(function(){
        var $img = jQuery(this);
        var imgID = $img.attr('id');
        var imgClass = $img.attr('class');
        var imgURL = $img.attr('src');
    
        jQuery.get(imgURL, function(data) {
            // Get the SVG tag, ignore the rest
            var $svg = jQuery(data).find('svg');
    
            // Add replaced image's ID to the new SVG
            if(typeof imgID !== 'undefined') {
                $svg = $svg.attr('id', imgID);
            }
            // Add replaced image's classes to the new SVG
            if(typeof imgClass !== 'undefined') {
                $svg = $svg.attr('class', imgClass+' replaced-svg');
            }
    
            // Remove any invalid XML tags as per http://validator.w3.org
            $svg = $svg.removeAttr('xmlns:a');
            
            // Check if the viewport is set, else we gonna set it if we can.
            if(!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
               // $svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'));
            }
    
            // Replace image with new SVG
            $img.replaceWith($svg);
    
        }, 'xml');
    
    });
});

// (function($) {
  
  j$(".page-id-32 .masonry_ctn > div").click(function(){
    var url = j$(this).find(".more").attr('href');
    window.location = url;
  });
  
// })(jQuery);


/* -------------- Sticky Sidebar Menu (2018-03-22) -------------- */
(function($){
  $(document).ready(function(){
    if($('.vc_span3 .widget_nav_menu').length) {
      var addLeftPos = function() {
        var sideNav = $('.vc_span3 .widget_nav_menu').eq(0);
        $(sideNav).removeClass('fixed-menu').removeAttr('style').css({
          left: $(sideNav).offset().left,
          width: $(sideNav).width()
        });
        stickySideNav();
      },
      stickySideNav = function() {
        var scrollPos = $(document).scrollTop(),
        footerTop = $('#footer_bg').offset().top - 200,
        sideNav = $('.vc_span3 .widget_nav_menu').eq(0),
        sideNavPos = $(sideNav).height() + 100 + scrollPos,
        sidebarTop = $('.vc_span3 .wpb_widgetised_column').eq(0).offset().top; /* 90px is sticky header height */
        if(scrollPos>sidebarTop) {
          $(sideNav).addClass('fixed-menu');
          if(sideNavPos>=footerTop) {
            $(sideNav).addClass('fade-away');
          }
          else {
            $(sideNav).removeClass('fade-away');
          }
        }
        else {
          $(sideNav).removeClass('fixed-menu');
        }
      }
      addLeftPos();
      stickySideNav();
      $(window).bind('resize orientationchange',addLeftPos);
      $(document).scroll(function(){
        stickySideNav();
      });
    }
  });
})(jQuery);
