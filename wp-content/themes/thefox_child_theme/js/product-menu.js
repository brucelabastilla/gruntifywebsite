(function($){
	$(document).ready(function(){
	    
	var formsBuild = jQuery('#forms').removeAttr('id');
	jQuery('<div id="forms" class="nav-region"></div>').insertBefore(formsBuild);
	formsBuild.nextUntil('#apps').appendTo('#forms');
	formsBuild.prependTo('#forms');

	var appsBuild = jQuery('#apps').removeAttr('id');
	jQuery('<div id="apps" class="nav-region"></div>').insertBefore(appsBuild);
	appsBuild.nextUntil('#maps').appendTo('#apps');
	appsBuild.prependTo('#apps');
	  
	var mapsBuild = jQuery('#maps').removeAttr('id');
	jQuery('<div id="maps" class="nav-region"></div>').insertBefore(mapsBuild);
	mapsBuild.nextUntil('#analytics').appendTo('#maps');
	mapsBuild.prependTo('#maps');	  
	    
	var analyticsBuild = jQuery('#analytics').removeAttr('id');
	jQuery('<div id="analytics" class="nav-region"></div>').insertBefore(analyticsBuild);
	analyticsBuild.nextUntil('#more-features').appendTo('#analytics');
	analyticsBuild.prependTo('#analytics');
    
    // 	var collect = jQuery('#collect').removeAttr('id');
    // 	jQuery('<div id="collect" class="nav-region"></div>').insertBefore(collect);
    // 	collect.nextUntil('#map').appendTo('#collect');
    // 	collect.prependTo('#collect');
    
    // 	var map = jQuery('#map').removeAttr('id');
    // 	jQuery('<div id="map" class="nav-region"></div>').insertBefore(map);
    // 	map.nextUntil('#action').appendTo('#map');
    // 	map.prependTo('#map');
    
    // 	var actionRegion = jQuery('#action').removeAttr('id');
    // 	jQuery('<div id="action" class="nav-region"></div>').insertBefore(actionRegion);
    // 	actionRegion.nextUntil('#more-features').appendTo('#action');
    // 	actionRegion.prependTo('#action');
    
    // 	var moreFeatures = jQuery('#more-features').removeAttr('id');
    // 	jQuery('<div id="more-features" class="nav-region"></div>').insertBefore(moreFeatures);
    // 	moreFeatures.next().appendTo('#more-features');
    // 	moreFeatures.prependTo('#more-features');
    
    // 	jQuery('.menu-item-11018,.menu-item-11019,.menu-item-11020,.menu-item-11021,.menu-item-11022','#menu-main-menu').removeClass('current-menu-item');	    
	    
	   // var navMenuId = {'plan-build':11018,'collect':11019,'map':11020,'action':11021,'more-features':11022},
	   // hash = window.location.hash,
	   // activeRegion = hash.replace('#','');
	    
	   // if(navMenuId[activeRegion] != undefined) jQuery('.menu-item-'+navMenuId[activeRegion]).addClass('current-menu-item');
	    
		stickyNav();
	    $(document).on('scroll', onScroll);
	    $('#product-menu a').on('click',function(e) {
	    	e.preventDefault();
	        $(document).off('scroll');
			$('#product-menu li a').removeClass('active');
	        $(this).addClass('active');
	        //console.log($(this).attr('class'));
	        var target = this.hash;
	        $('html, body').stop().animate({
	            'scrollTop': $(target).offset().top + 0
	        }, 1000, 'swing', function () {
	            window.location.hash = target;
	            $(document).on('scroll',onScroll);
	        });
	    });
	});
	var stickyNav = function() {
		var sectPos = $('#product-menu-section').offset().top,
		scrollPos = $(document).scrollTop();
	    if(scrollPos>sectPos) {
	    	$('#product-menu-section').addClass('fixed-me');
	    }
	    else {
	    	$('#product-menu-section').removeClass('fixed-me');
	    }
	},
	onScroll = function() {
	    var scrollPos = $(document).scrollTop();
	    $('#product-menu a').each(function() {
	        var refId = $(this).attr('href');
	        if($(refId).length) {
	        var refTop = $(refId).position().top - 10;
    	        if (refTop <= scrollPos && $(refId).position().top + $(refId).height() > scrollPos) {
    	            $('#product-menu li a').removeClass('active');
    	            $(this).addClass('active');
    	        }
    	        else{
    	            $(this).removeClass('active');
    	        }    
    	        console.log('x');
	        }
	    });
	    stickyNav();
	}
})(jQuery);