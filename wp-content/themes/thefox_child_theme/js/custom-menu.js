jQuery(document).ready(function(){
    
    var customMenu = jQuery('<div/>',{
        id: 'morph-header',
        class: 'cd-morph-dropdown'
    }),
    subLiIndex = 0;
    jQuery('<nav/>',{
        class: 'main-nav'
    }).appendTo(customMenu);
    jQuery('<ul/>',{
        class: 'main-nav'
    }).appendTo(jQuery('nav',customMenu));
    customMenu.append('<div class="morph-dropdown-wrapper"><div class="dropdown-list"><ul></ul></div></div>');
    jQuery('#menu-main-menu > li').each(function(i){
        var a = jQuery('>a',this);
        if(jQuery(this).hasClass('menu-item-has-children')) {
            var liId = 'li'+subLiIndex;
            subLiIndex++;
            jQuery('nav ul',customMenu).append('<li class="has-dropdown gallery '+jQuery(this).attr('class')+'" data-content="'+liId+'"><a href="'+a.attr('href')+'">'+a.html()+'</a></li>');
            jQuery('.dropdown-list > ul',customMenu).append('<li id="'+liId+'" class="dropdown gallery"><a href="#'+liId+'" class="label">'+a.html()+'</a><div class="content"></div></li>');
            var submenu = jQuery('>ul',this).clone();
            jQuery('#'+liId+' .content',customMenu).append(submenu);
        }
        else {
            jQuery('nav ul',customMenu).append('<li><a class="'+a.attr('class')+'" href="'+a.attr('href')+'">'+a.html()+'</a></li>');
        }
    });
    var li1HalfCount = Math.ceil(jQuery('#li1 .content ul.sub-menu li',customMenu).length/2);
    jQuery('<ul/>',{
        class: 'left-ul'
    }).appendTo(jQuery('#li1 .content',customMenu));
    jQuery('<ul/>',{
        class: 'right-ul'
    }).appendTo(jQuery('#li1 .content',customMenu));
    jQuery('#li1 .content ul.sub-menu li',customMenu).each(function(i){
        if(i>=li1HalfCount) {
            jQuery('#li1 .content .right-ul',customMenu).append(this);
        }
        else {
            jQuery('#li1 .content .left-ul',customMenu).append(this);
        }
    });
    jQuery('#li1 .content .sub-menu',customMenu).remove();
    customMenu.appendTo('#header_container > header.nav_type_1 > .wrapper');
    
    setTimeout(function(){
    
        function morphDropdown(element) {
            this.element = element;
            this.mainNavigation = this.element.find('.main-nav');
            this.mainNavigationItems = this.mainNavigation.find('.has-dropdown');
            this.dropdownList = this.element.find('.dropdown-list');
            this.dropdownWrappers = this.dropdownList.find('.dropdown');
            this.dropdownItems = this.dropdownList.find('.content');
            this.dropdownBg = this.dropdownList.find('.bg-layer');
            this.mq = this.checkMq();
            this.bindEvents();
        }
        morphDropdown.prototype.checkMq = function() {
            var self = this;
            return window.getComputedStyle(self.element.get(0), '::before').getPropertyValue('content').replace(/'/g, "").replace(/"/g, "").split(', ');
        };
        morphDropdown.prototype.bindEvents = function() {
            var self = this;
            this.mainNavigationItems.mouseenter(function(event) {
                self.showDropdown(jQuery(this));
            }).mouseleave(function() {
                setTimeout(function() {
                    if (self.mainNavigation.find('.has-dropdown:hover').length == 0 && self.element.find('.dropdown-list:hover').length == 0) self.hideDropdown();
                }, 50);
            });
            this.dropdownList.mouseleave(function() {
                setTimeout(function() {
                    (self.mainNavigation.find('.has-dropdown:hover').length == 0 && self.element.find('.dropdown-list:hover').length == 0) && self.hideDropdown();
                }, 50);
            });
            this.mainNavigationItems.on('touchstart', function(event) {
                var selectedDropdown = self.dropdownList.find('#' + jQuery(this).data('content'));
                if (!self.element.hasClass('is-dropdown-visible') || !selectedDropdown.hasClass('active')) {
                    event.preventDefault();
                    self.showDropdown(jQuery(this));
                }
            });
            this.element.on('click', '.nav-trigger', function(event) {
                event.preventDefault();
                self.element.toggleClass('nav-open');
            });
        };
        morphDropdown.prototype.showDropdown = function(item) {
            this.mq = this.checkMq();
            if (this.mq == 'desktop') {
                var self = this;
                var selectedDropdown = this.dropdownList.find('#' + item.data('content')),
                    selectedDropdownHeight = selectedDropdown.innerHeight(),
                    selectedDropdownWidth = selectedDropdown.children('.content').innerWidth(),
                    selectedDropdownLeft = item.offset().left + item.innerWidth() / 2 - selectedDropdownWidth / 2;
    
    var ww = jQuery(window).width(),
    hww = jQuery('#header_container > header.nav_type_1 > .wrapper').width(),
    adjustLeft = (ww-hww)/2;
    
    
                this.updateDropdown(selectedDropdown, parseInt(selectedDropdownHeight), selectedDropdownWidth, parseInt(selectedDropdownLeft-adjustLeft));
                this.element.find('.active').removeClass('active');
                selectedDropdown.addClass('active').removeClass('move-left move-right').prevAll().addClass('move-left').end().nextAll().addClass('move-right');
                item.addClass('active');
                if (!this.element.hasClass('is-dropdown-visible')) {
                    setTimeout(function() {
                        self.element.addClass('is-dropdown-visible');
                    }, 10);
                }
            }
        };
        morphDropdown.prototype.updateDropdown = function(dropdownItem, height, width, left) {
    
            this.dropdownList.css({
                '-moz-transform': 'translateX(' + left + 'px)',
                '-webkit-transform': 'translateX(' + left + 'px)',
                '-ms-transform': 'translateX(' + left + 'px)',
                '-o-transform': 'translateX(' + left + 'px)',
                'transform': 'translateX(' + left + 'px)',
                'width': width + 'px',
                'height': height + 'px'
            });
            this.dropdownBg.css({
                '-moz-transform': 'scaleX(' + width + ') scaleY(' + height + ')',
                '-webkit-transform': 'scaleX(' + width + ') scaleY(' + height + ')',
                '-ms-transform': 'scaleX(' + width + ') scaleY(' + height + ')',
                '-o-transform': 'scaleX(' + width + ') scaleY(' + height + ')',
                'transform': 'scaleX(' + width + ') scaleY(' + height + ')'
            });
        };
        morphDropdown.prototype.hideDropdown = function() {
            this.mq = this.checkMq();
            if (this.mq == 'desktop') {
                this.element.removeClass('is-dropdown-visible').find('.active').removeClass('active').end().find('.move-left').removeClass('move-left').end().find('.move-right').removeClass('move-right');
            }
        };
        morphDropdown.prototype.resetDropdown = function() {
            this.mq = this.checkMq();
            if (this.mq == 'mobile') {
                this.dropdownList.removeAttr('style');
            }
        };
        var morphDropdowns = [];
        if (jQuery('.cd-morph-dropdown').length > 0) {
            jQuery('.cd-morph-dropdown').each(function() {
                morphDropdowns.push(new morphDropdown(jQuery(this)));
            });
            var resizing = false;
            updateDropdownPosition();
            jQuery(window).on('resize', function() {
                if (!resizing) {
                    resizing = true;
                    (!window.requestAnimationFrame) ? setTimeout(updateDropdownPosition, 300): window.requestAnimationFrame(updateDropdownPosition);
                }
            });
    
            function updateDropdownPosition() {
                morphDropdowns.forEach(function(element) {
                    element.resetDropdown();
                });
                resizing = false;
            };
        }
    
    },100);    
    
});