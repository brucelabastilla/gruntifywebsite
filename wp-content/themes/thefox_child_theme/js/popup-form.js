jQuery(document).ready(function(){
	jQuery('.open-dialog').each(function(){
		jQuery(this).click(function(e){
			e.preventDefault();
			var modalId = '#gform-'+jQuery(this).data('form_id');
			jQuery(modalId).css({
				'position': 'absolute',
				'left': '9999px',
			}).show();
			var h = jQuery('>.modal__content',modalId).outerHeight(true),
			wh = jQuery(window).height(),
			top = 0;
			if(wh>h) top = Math.ceil((wh-h)/2);
			jQuery('>.modal__content',modalId).css('top',top+'px');
			jQuery(modalId).removeAttr('style').fadeIn();
		});
	});
});
jQuery(window).resize(function(){
	var modal = jQuery('.modal:visible');
	//console.log(modal.length);
	if(modal.length) {
		var h = jQuery('>.modal__content',modal).outerHeight(true),
		wh = jQuery(window).height(),
		top = 0;
		if(wh>h) top = Math.ceil((wh-h)/2);
		jQuery('>.modal__content',modal).css('top',top+'px');		
	}
});