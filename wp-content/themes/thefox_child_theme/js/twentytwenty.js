var resizer = function() {
	var leftCol = jQuery('#before-after').closest('.vc_column-inner'),
	leftColH = leftCol.outerHeight(true),
	leftColW = leftCol.outerWidth(true),
	bah = jQuery('#before-after').height(),
	h9 = leftColH/9,
	w16 = leftColW/16;
	if(h9>w16) {
		var nbaw = leftColH / 9 * 16,
		negMarginLeft = (leftColW-nbaw)/2,
		offsetMargin = Math.abs(negMarginLeft);
		jQuery('#before-after').height(leftColH).width(nbaw).css({
			marginLeft: negMarginLeft,
			marginTop: 'auto',
			minHeight: 'auto'
		});
		jQuery('#before-after .twentytwenty-before-label').css('margin-left',offsetMargin);
		jQuery('#before-after .twentytwenty-after-label').css('margin-left',negMarginLeft);
	}
	else {
		var nbah = leftColW / 16 * 9,
		negMarginTop = (leftColH-nbah)/2;
		console.log(nbah);
		jQuery('#before-after').height(nbah).width(leftColW).css({
			minHeight: nbah,
			marginLeft: 'auto',
			marginTop: negMarginTop
		});
		jQuery('#before-after .twentytwenty-before-label,#before-after .twentytwenty-after-label').removeAttr('style');
	}
}
jQuery(document).ready(function(){
	jQuery('#before-after').twentytwenty({default_offset_pct: 0.5});
	resizer();
});
jQuery(window).resize(function(){
	resizer();
}).bind('resize orientationchange',resizer);
