var timeLine = function() {
	if(!jQuery('#timeline').length) return;
	jQuery('#timeline').addClass('run');
	var time = 600,
	i = 0;
	jQuery('.tl-cell').each(function(){
		var tlCell = this;
  		setTimeout( function(){ 
  			jQuery(tlCell).addClass('go');
  		}, time )
  		time += 500 + ( i*10 );
  		i++;
	});		
},
alignMe = function() {
	var tlw = jQuery('#timeline').width(),
	tllw = jQuery('#timeline .tl-line').width();
	if(tlw<=tllw) {
		var what = -1*((tllw-tlw)/2);
		jQuery('#timeline .tl-line').css('margin-left',what);
	}
	else {
		jQuery('#timeline .tl-line').removeAttr('style');
	}	
}
jQuery(document).ready(function(){
	alignMe();
});
jQuery(window).resize(function(){
	alignMe();
});
jQuery('#timeline').waypoint(
	function() { 
		timeLine();
	},
	{offset:'100%'}
);