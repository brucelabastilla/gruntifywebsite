<?php
define( 'THEME_NAME', 'gruntify' );
if( ! class_exists( 'RDFrameworkMetaboxes' ) ) {
        include( get_template_directory()  . '/includes/metaboxes/metaboxes.php' );
}
class jobs extends RDFrameworkMetaboxes {

        function __construct() {
                add_action( 'init' , [ $this, 'register' ] );
                add_filter( 'manage_job_posts_columns', [ $this, 'columns' ] );
                add_action( 'manage_job_posts_custom_column' , [ $this, 'custom_columns' ], 10, 2 );
                add_action( 'add_meta_boxes',[ $this, 'register' ] );
                add_action( 'save_post_job', [ $this, 'save' ], 1, 3 );
                //add_action( 'after_switch_theme', [ $this, 'schedule_event' ] );
                //add_action( 'hourly_event', [ $this,'close_jobs' ] );
                //add_filter( '__after_page_title', [ $this, 'show_categories' ] );
                add_shortcode( 'jobs', [ $this, 'jobs' ] );
                add_shortcode( 'jobheader', [ $this, 'job_header' ] );
        }

        function job_status( $lang ) {
                $job_status = [
                        'open' => __( 'Open', THEME_NAME ),
                        'closed' => __( 'Closed', THEME_NAME )
                ];
                return $job_status[$lang];
        }

        static function job_status_alt( $lang ) {
                $job_status = [
                        'open' => __( 'Open-ended', THEME_NAME ),
                        'closed' => __( 'Close-ended', THEME_NAME )
                ];
                return $job_status[$lang];
        }

        function register() {
                register_post_type( 'job',
                        [
                                'hierarchical' => false,
                                'label' => __( 'Jobs', THEME_NAME ),
                                'labels' => [
                                        'name' => __( 'Jobs', THEME_NAME ),
                                        'singular_name' => __( 'Job', THEME_NAME ),
                                        'add_new_item' => __( 'Add New Job', THEME_NAME ),
                                        'edit_item' => __( 'Edit Job', THEME_NAME ),
                                        'new_item' => __( 'New Job', THEME_NAME ),
                                        'view_item' => __( 'View Job', THEME_NAME ),
                                        'all_items' => __( 'All Jobs', THEME_NAME ),
                                        'archives' => __( 'Jobs', THEME_NAME ),
                                        'view_item' => __( 'View Job', THEME_NAME ),
                                        'search_items' => __( 'Search Jobs', THEME_NAME ),
                                        'not_found' =>  __( 'No jobs found', THEME_NAME ),
                                        'not_found_in_trash' => __( 'No jobs found in Trash', THEME_NAME ) 
                                ],
                                'public' => true,
                                'publicly_queryable' => true,
                                'show_ui' => true, 
                                'show_in_nav_menus' => true,
                                'show_in_menu' => true, 
                                'rewrite' => [ 'slug' => 'job' ],
                                'has_archive' => false, 
                                //'menu_position' => 8,
                                'menu_icon' => 'dashicons-groups',
                                'supports' => [ 'title', 'editor', 'author', 'page_options' ],
                                'register_meta_box_cb' => [ $this, 'meta_boxes' ]
                        ]
                );
                register_taxonomy( 'job_cat', 'job', array(
                        'labels' => [
                                'name' => _x( 'Job Categories', 'taxonomy general name', THEME_NAME ),
                                'singular_name' => _x( 'Job Category', 'taxonomy singular name', THEME_NAME ),
                                'search_items' =>  __( 'Search Job Categories', THEME_NAME ),
                                'all_items' => __( 'All Job Categories', THEME_NAME ),
                                'parent_item' => __( 'Parent Job Category', THEME_NAME ),
                                'parent_item_colon' => __( 'Parent Job Category:', THEME_NAME ),
                                'edit_item' => __( 'Edit Job Category', THEME_NAME ), 
                                'update_item' => __( 'Update Job Category', THEME_NAME ),
                                'add_new_item' => __( 'Add New Job Category', THEME_NAME ),
                                'new_item_name' => __( 'New Job Category', THEME_NAME ),
                                'menu_name' => __( 'Job Categories', THEME_NAME )
                        ],
                        'show_ui' => true,
                        'query_var' => true,
                        'hierarchical' => true
                ));
        }

        function columns( $columns ) {
                $date_column = $columns['date'];
                unset( $columns['date'] );
                //$columns['closing_date'] = __( 'Closing Date', THEME_NAME );
                $columns['label'] = __( 'Label', THEME_NAME );
                //$columns['job_status'] = __( 'Status', THEME_NAME );
                $columns['date'] = $date_column;
                return $columns;
        }

        function custom_columns( $column_name, $post_id ) {
                switch ( $column_name ) {
                        // case 'closing_date':
                        //         $closing_date = get_post_meta( $post_id, 'closing_date', true ); 
                        //         if( ! empty( $closing_date ) )  echo date( 'F j, Y', $closing_date );
                        // break;  
                        case 'label':
                                echo get_post_meta( $post_id, 'closing_date_label', true ); 
                        break; 
                        case 'job_status':
                                $job_status = get_post_meta( $post_id, 'job_status', true ); 
                                if( ! empty( $job_status ) ) echo $this->job_status( $job_status );
                        break;                                                  
                }
        }

        function meta_boxes() {
                wp_enqueue_style( 'font-icons', get_stylesheet_directory_uri() . '/jobs/font-icons.css', [], '1.0' );
                wp_enqueue_style( 'datepicker', get_stylesheet_directory_uri() . '/jobs/datepicker.css', [], '1.0' );
                wp_enqueue_script( 'jquery-ui-datepicker' );
                wp_enqueue_script( 'datepicker', get_stylesheet_directory_uri() . '/jobs/datepicker.js', [ 'jquery','jquery-ui-datepicker' ], '1.0', true );
                add_meta_box(
                        'page_options', 
                        __( 'Page Options', THEME_NAME ),
                        [ $this, 'page_options_meta_box' ], 
                        'job', 
                        'normal',
                        'default'
                );
                add_meta_box(
                        'closing_date', 
                        __( 'Subtitle', THEME_NAME ),
                        [ $this, 'closing_date_meta_box' ], 
                        'job', 
                        'side',
                        'low'
                );
                // add_meta_box(
                //         'job_status', 
                //         __( 'Status', THEME_NAME ),
                //         [ $this, 'job_status_meta_box' ], 
                //         'job', 
                //         'side',
                //         'low'
                // );
        }

        function page_options_meta_box() {
                include( get_template_directory()  . '/includes/metaboxes/views/metaboxes/style.php' );
                include( get_template_directory() . '/includes/metaboxes/views/metaboxes/page_options.php' );                
        }

        function job_status_meta_box( $post ) {
                $job_status = get_post_meta( $post->ID, 'job_status', true ); 
                if( empty( $job_status ) ) $job_status = 'open'; ?>
                <strong class="<?php echo $job_status; ?>"><?php echo $this->job_status( $job_status ); ?></strong>
                <?php
        }

        function closing_date_meta_box( $post ) { 
                $closing_date = get_post_meta( $post->ID, 'closing_date', true ); 
                $closing_date_label = get_post_meta( $post->ID, 'closing_date_label', true ); 
                if( empty( $closing_date ) ) $closing_date = strtotime( '+1 month' ); ?>
                <p style="display:none">
                    <strong><label for="closing_date"><?php _e( 'Date', THEME_NAME ); ?></label></strong>
                </p>
                <p style="display:none">
                    <input type="text" id="closing-date" name="closing_date" class="datepicker" readonly="readonly" value="<?php echo date( 'F j, Y', $closing_date ); ?>">
                </p>
                <p>
                    <strong><label for="closing_date_label"><?php _e( 'Label', THEME_NAME ); ?></label></strong>
                </p>
                <p>
                    <input type="text" id="closing_date_label" name="closing_date_label" value="<?php echo $closing_date_label; ?>" class="large-text">
                </p>
                <?php
        }

        function save( $post_ID, $post, $update ) {
                if( isset( $_POST['closing_date'] ) ) {
                        $closing_timestamp = strtotime( $_POST['closing_date'] );
                        update_post_meta( $post_ID, 'closing_date', $closing_timestamp );
                        $current_day_timestamp = strtotime( date( 'Y-m-d', current_time( 'timestamp' ) ) );
                        $job_status = ( $closing_timestamp <= $current_day_timestamp )? 'closed': 'open';
                        update_post_meta( $post_ID, 'job_status', $job_status );
                }             
                if( isset( $_POST['closing_date_label'] ) ) {
                        update_post_meta( $post_ID, 'closing_date_label', sanitize_text_field( $_POST['closing_date_label'] ) );
                }   
        }

        // function schedule_event() {
        //         if( ! wp_next_scheduled ( 'hourly_event' ) ) {
        //                 wp_schedule_event( time(), 'hourly', 'hourly_event' );
        //         }
        // }

        // function close_jobs() {
        //         global $wpdb;
        //         $current_day_timestamp = strtotime( date( 'Y-m-d', current_time( 'timestamp' ) ) );
        //         $week_ago_timestamp = $current_day_timestamp - 604800;
        //         $sql = "SELECT post_id FROM " . $wpdb->prefix . "postmeta WHERE meta_key LIKE 'closing_date' AND CAST(meta_value AS UNSIGNED) < $current_day_timestamp AND CAST(meta_value AS UNSIGNED) > $week_ago_timestamp";
        //         $results = $wpdb->get_results( $sql );
        //         if( empty( $results ) ) return;
        //         $closed_job_ids = [];
        //         foreach( $results as $closed_job ) {
        //                 //update_post_meta( $closed_job->post_id, 'job_status', 'closed' );
        //                 $closed_job_ids[] = $closed_job->post_id;
        //         }
        //         $wpdb->query( "UPDATE " . $wpdb->prefix . "postmeta SET meta_value = 'closed' WHERE meta_key LIKE 'job_status' AND post_id IN (" . implode( $closed_job_ids, ',' ) . ")" );
        // }

        static function get_job_cats( $post_id ) {
                $job_cats = get_the_terms( $post_id, 'job_cat' );
                if( empty( $job_cats ) ) return;
                $terms = [];
                foreach( $job_cats as $job_cat ) {
                        $terms[] = '<span class="job_cat button ' . $job_cat->slug . '">' . $job_cat->name . '</span>';
                }
                return $terms;
        }

        function show_categories() {
                global $post_type;
                if( $post_type != 'job' ) return;
                global $post;
                //$terms = get_the_term_list( $id, 'job_cat' );
                $terms = jobs::get_job_cats( $post->ID );
                $closing_date_label = get_post_meta( $post->ID, 'closing_date_label', true );
                if( empty( $job_status ) ) $job_status = 'open'; ?>
                <div id="job-header" class="job-header section-gradient-angle">
                        <div class="wrapper section_wrapper">
                                <h3 class="job-title"><?php the_title(); ?></h3>
                                <?php if( $closing_date_label ): ?>
                                <p class="job-subtitle"><?php echo $closing_date_label; ?></p>
                                <?php endif; ?>
                                <?php echo implode( $terms, ',' ); ?>
                        </div>
                </div>
                <?php
        }

        function job_header() {
                global $post_type;
                if( $post_type != 'job' ) return;
                global $post;
                //$terms = get_the_term_list( $id, 'job_cat' );
                $terms = jobs::get_job_cats( $post->ID );
                $closing_date_label = get_post_meta( $post->ID, 'closing_date_label', true );
                if( empty( $job_status ) ) $job_status = 'open'; ?>
                <div class="job-header">
                        <div class="wrapper section_wrapper">
                                <h3 class="job-title"><?php the_title(); ?></h3>
                                <?php if( $closing_date_label ): ?>
                                <p class="job-subtitle"><?php echo $closing_date_label; ?></p>
                                <?php endif; ?>
                                <?php echo implode( $terms, ',' ); ?>
                        </div>
                </div>
                <?php
        }

        function jobs() {
                global $wp_query;
                $args = [
                            'paged' => max( 1, get_query_var( 'paged' ) ),
                            'post_type' => 'job'
                            // 'meta_key' => 'job_status',
                            // 'meta_value' => 'open'
                ];
                $wp_query = new WP_Query( $args ); 
                wp_enqueue_style( 'xygrid', get_stylesheet_directory_uri() . '/jobs/xygrid.css', [], '6.4.3' );
                wp_enqueue_style( 'jobs', get_stylesheet_directory_uri() . '/jobs/jobs.css', [], '6.4.3' );
                ob_start(); ?>
                <div class="grid-x grid-margin-x grid-margin-y small-up-1 large-up-3">
                <?php if ( have_posts() ):
                        while ( have_posts() ) : the_post();
                                        $post_id = get_the_ID();
                                        $terms = jobs::get_job_cats( $post_id );
                                        $closing_date_label = get_post_meta( $post_id, 'closing_date_label', true );  ?>
                        <div class="cell">
                                <a class="job-card" href="<?php echo get_permalink(); ?>">
                                        <div class="job-title-wrap">
                                                <h3>
                                                                <?php the_title(); ?>
                                                </h3>
                                                <?php if( $closing_date_label ): ?>
                                                <h5 class="job-status"><?php echo $closing_date_label; ?></h5>
                                                <?php endif; ?>
                                        </div>
                                        <?php if( $terms ):
                                                echo implode( $terms, ',' );
                                        endif; ?>
                                </a>
                        </div>
                        <?php endwhile; ?>
                                <div class="navigation clearfix">
                                <?php kriesi_pagination(); ?>
                                </div>
                <?php else: ?>

                <?php endif; ?>
                </div>
                <?php wp_reset_query();
                return ob_get_clean();
        }

}
new jobs;