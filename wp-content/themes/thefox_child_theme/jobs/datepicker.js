(function($){
    $(function() {
		$('.datepicker').each(function(){
			var params = {
				changeYear: true 
			};
			if(typeof($(this).data('date-format'))!=='undefined'){
				params.dateFormat = $(this).data('date-format');
			}
			if(typeof($(this).data('year-range'))!=='undefined'){
				params.yearRange = $(this).data('year-range');
			}
			if(typeof($(this).data('min-date'))!=='undefined'){
				params.minDate = $(this).data('min-date');
			}
			if(typeof($(this).data('max-date'))!=='undefined'){
				params.maxDate = $(this).data('max-date');
			}
			$(this).datepicker(params).keyup(function(e) {
			    if(e.keyCode == 8 || e.keyCode == 46) {
			        $.datepicker._clearDate(this);
			    }
			});
		});
    });
})(jQuery);